﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;


namespace DIAMANTE.BussinesLogic
{
    public class RolBL
    {

        #region Constructors

        private static RolDA rolObj;

        public RolBL()
        {
            rolObj = new RolDA();
        }

        #endregion

        #region No Transaccionales
        public List<RolBE> ObtenerxUsuario(String usuario)
        {
            return rolObj.ObtenerxUsuario(usuario);
        }
        public List<RolBE> ObtenerLista()
        {
            return rolObj.ObtenerLista();
        }
        public List<UsuarioRolBE> Obtener_Roles_User(String Cod_Pers)
        {
            return new RolDA().Obtener_Roles_User(Cod_Pers);
        }
        public RolBE ObtenerRolxId(int rol_id)
        {
            return new RolDA().ObtenerRolxId(rol_id);
        }
        #endregion

        #region Transaccionales
        public String RegistrarRol(RolBE objRol)
        {
            return new RolDA().RegistrarRol(objRol);
        }
        public string RegistrarRolOpcion(int rol_id, int opci_id)
        {
            return new RolDA().RegistrarRolOpcion(rol_id, opci_id);
        }
        #endregion

        public List<OpcionBE> ObtenerOpcionesxIdRol(int rol_id)
        {
            return new RolDA().ObtenerOpcionesxIdRol(rol_id);
        }

        public void Limpiar_Rol_Opcion(int rol_id)
        {
            new RolDA().Limpiar_Rol_Opcion(rol_id);
        }

        public void Actualizar_Rol(RolBE objRol)
        {
            new RolDA().Actualizar_Rol(objRol);
        }


    }
}
