﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;
namespace DIAMANTE.BussinesLogic
{
    public class PreparacionBL
    {
         #region Constructors

        private static PreparacionDA Registro_PreparacionObj;

        public PreparacionBL() {
            Registro_PreparacionObj = new PreparacionDA();
        }

        #endregion

        #region Methods
        public List<PreparacionBE> ObtenerInfoLista()
        {
            return Registro_PreparacionObj.Listar_Preparacion();
        }

        //public List<PreparacionBE> ObtenerInfoRegistro(Int32 ID)
        //{
        //    return Registro_PreparacionObj.Listar_Registro(ID);
        //}


        public String Insertar(PreparacionBE oBeIns)
        {
            return Registro_PreparacionObj.Insertar(oBeIns);
        }

        //public String Completar(Registro_IngresoBE oBeIns)
        //{
        //    return Registro_IngresoObj.Completar(oBeIns);
        //}
        #endregion
    }
}
