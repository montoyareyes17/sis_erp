﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public class PrevbBL
    {
        private static PrevbDA RegistroObj;

        public PrevbBL()
        {
            RegistroObj = new PrevbDA();
        }

        public String Insertar(PrevbBE oBeIns)
        {
            return RegistroObj.Insertar(oBeIns);
        }

        public List<PrevbBE> ObtenerInfoLista()
        {
            return RegistroObj.Listar_Prevb();
        }


    }
}
