﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;


namespace DIAMANTE.BussinesLogic
{
    public class Registro_Ingreso_DetalleBL
    {
        #region Constructors

        private static Registro_Ingreso_DetalleDA Registro_IngresoDObj;

        public Registro_Ingreso_DetalleBL()
        {
            Registro_IngresoDObj = new Registro_Ingreso_DetalleDA();
        }

        
        #endregion

        #region Methods
        public List<Registro_Ingreso_DetalleBE> ObtenerInfoLista(Int32 ID_REGISTRO)
        {
            return Registro_IngresoDObj.Listar_Registro_Ingreso_Detalle(ID_REGISTRO);
        }

       
        public List<Registro_Ingreso_DetalleBE> ObtenerInfo(Int32 ID)
        {
            return Registro_IngresoDObj.Listar_Registro_Detalle(ID);
        }

        public String Insertar(Registro_Ingreso_DetalleBE oBeIns)
        {
            return Registro_IngresoDObj.Insertar(oBeIns);
        }
        #endregion

    }
}
