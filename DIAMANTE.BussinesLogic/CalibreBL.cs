﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
   public class CalibreBL
    {
    #region Constructors

        private static CalibreDA calibreObj;
        public CalibreBL()
        {
            calibreObj = new CalibreDA();
        }
        
     #endregion

        public List<CalibreBE> ObtenerInfoLista()
        {
            return calibreObj.Listar_Calibre();
        }

    }
}
