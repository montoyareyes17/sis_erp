﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public class EspecieBL
    {

        private static EspecieDA especieObj;
        public EspecieBL() {
            especieObj = new EspecieDA();
        }
        public List<EspecieBE> ObtenerInfoLista(Int32 cliente)
        {
            return especieObj.Listar_Especie(cliente);
        }

    }
}
