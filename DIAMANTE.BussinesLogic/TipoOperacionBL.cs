﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;
namespace DIAMANTE.BussinesLogic
{
    public class TipoOperacionBL
    {
        private static TipoOperacionDA TipoOperacionObj;
        public TipoOperacionBL()
        {
            TipoOperacionObj = new TipoOperacionDA();
        }

        public List<TipoOperacionBE> ObtenerInfoLista(Int32 estado)
        {
            return TipoOperacionObj.Listar_Tipo_Operacion(estado);
        }
    }
}
