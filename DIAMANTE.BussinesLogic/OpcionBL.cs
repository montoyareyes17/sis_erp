﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public class OpcionBL
    {
        #region Constructors

        private static OpcionDA opcionObj;

        public OpcionBL()
        {
            opcionObj = new OpcionDA();
        }

        #endregion

        #region Methods

        public List<OpcionBE> ObtenerxRol(Int32 rol, Int32 padre)
        {
            return opcionObj.ObtenerxRol(rol, padre);
        }

        public List<OpcionBE> ObtenerTodos()
        {
            return opcionObj.ObtenerTodos();
        }

        public List<OpcionBE> ObtenerOpcionPadre()
        {
            return opcionObj.ObtenerOpcionPadre();
        }

        public List<OpcionBE> ObtenerxPadre(Int32 padre)
        {
            return opcionObj.ObtenerxPadre(padre);
        }

        public String Insertar(OpcionBE oBe)
        {
            return opcionObj.Insertar(oBe);
        }

        public String Eliminar(OpcionBE oBe)
        {
            return opcionObj.Eliminar(oBe);
        }

        public String Actualizar(OpcionBE oBe)
        {
            return opcionObj.Actualizar(oBe);
        }

        #endregion
    }
}
