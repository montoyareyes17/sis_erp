﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public class ProTurnoBL
    {
        #region Constructors

        private static ProTurnoDA Registro_Obj;

        public ProTurnoBL()
        {
            Registro_Obj = new ProTurnoDA();
        }


        public String Insertar(ProTurnoBE oBeIns)
        {
            return Registro_Obj.Insertar(oBeIns);
        }

        public List<ProTurnoBE> ObtenerInfoLista()
        {
            return Registro_Obj.Listar_ProTurno();
        }
        #endregion


    }
}
