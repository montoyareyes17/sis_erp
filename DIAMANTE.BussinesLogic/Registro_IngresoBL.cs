﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public class Registro_IngresoBL
    {
        #region Constructors

        private static Registro_IngresoDA Registro_IngresoObj;

        public Registro_IngresoBL() {
            Registro_IngresoObj = new Registro_IngresoDA();
        }

        #endregion

        #region Methods
        public List<Registro_IngresoBE> ObtenerInfoLista(Int32 TIPO_INGRESO)
        {
            return Registro_IngresoObj.Listar_Registro_Ingreso(TIPO_INGRESO);
        }

        public List<Registro_IngresoBE> ObtenerInfoRegistro(Int32 ID)
        {
            return Registro_IngresoObj.Listar_Registro(ID);
        }


        public String Insertar(Registro_IngresoBE oBeIns)
        {
            return Registro_IngresoObj.Insertar(oBeIns);
        }

        public String Completar(Registro_IngresoBE oBeIns)
        {
            return Registro_IngresoObj.Completar(oBeIns);
        }
        #endregion

    }
}
