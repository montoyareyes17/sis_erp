﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public class EmpleadoBL
    {
        #region Constructors

        private static EmpleadoDA empleadoObj;

        public EmpleadoBL()
        {
            empleadoObj = new EmpleadoDA();
        }

        #endregion

        #region Methods
        public EmpleadoBE ObtenerInfo(String usuario, Int32 rol)
        {
            return empleadoObj.ObtenerInfo(usuario, rol);
        }

        public List<EmpleadoBE> ObtenerInfoLista(Int32 SEDE_ID)
        {
            return empleadoObj.ObtenerInfoLista(SEDE_ID);
        }

        public List<EmpleadoBE> Obtener_Docentes(int sede_id)
        {
            return empleadoObj.Obtener_Docentes(sede_id);
        }

        #endregion
    }
}
