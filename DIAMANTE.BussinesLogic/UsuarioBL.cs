﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public class UsuarioBL
    {
        #region Constructors

        private static UsuarioDA usuarioObj;
        public UsuarioBL()
        {
            usuarioObj = new UsuarioDA();
        }

        #endregion

        #region Methods

        public string ValidarUsuario(UsuarioBE oBe)
        {
            return usuarioObj.ValidarUsuario(oBe);
        }

        #endregion

    }
}
