﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;


namespace DIAMANTE.BussinesLogic
{
   public class PresentacionBL
    {
       private static PresentacionDA presentacionObj;
       public PresentacionBL() {
           presentacionObj = new PresentacionDA();
       }

       public List<PresentacionBE> ObtenerInfoLista(Int32 especie)
       {
           return presentacionObj.Listar_Presentacion(especie);
       }

       public List<PresentacionBE> ObtenerInfoLista_Catalogo(Int32 cliente)
       {
           return presentacionObj.Listar_Presentacion_Catalogo(cliente);
       }


    }
}
