﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public class StockBL
    {
            #region Constructors

        private static StockDA STOCKObj;
        public StockBL()
        {
            STOCKObj = new StockDA();
        }
        
     #endregion


        public List<StockBE> ObtenerInfoLista()
        {
            return STOCKObj.Listar_Stock();
        }

    }
}
