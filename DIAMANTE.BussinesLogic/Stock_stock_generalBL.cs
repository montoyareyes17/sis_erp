﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public class Stock_stock_generalBL
    {
        private static Stock_stock_generalDA stock_GeneralObj;

        public Stock_stock_generalBL()
        {

            stock_GeneralObj = new Stock_stock_generalDA();
        }

        public List<Stock_stock_generalBE> ObtenerInfoLista()
        {
            return stock_GeneralObj.Listar_General();
        }

    }
}
