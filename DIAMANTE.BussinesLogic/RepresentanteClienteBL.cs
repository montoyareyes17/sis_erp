﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;
namespace DIAMANTE.BussinesLogic
{
    public class RepresentanteClienteBL
    {
        private static RepresentanteClienteDA RepresentanteObj;
        public RepresentanteClienteBL()
        {
            RepresentanteObj = new RepresentanteClienteDA();
        }
        public List<RepresentanteClienteBE> ObtenerInfoLista(Int32 cliente)
        {
            return RepresentanteObj.Listar_Representante(cliente);
        }
    }
}
