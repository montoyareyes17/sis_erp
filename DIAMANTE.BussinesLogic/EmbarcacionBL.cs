﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using DIAMANTE.DataAccess;

namespace DIAMANTE.BussinesLogic
{
    public  class EmbarcacionBL
    {
                #region Constructors

        private static EmbarcacionDA embarcacionObj;

        public EmbarcacionBL()
        {
            embarcacionObj = new EmbarcacionDA();
        }
        
        #endregion

        public List<EmbarcacionBE> ObtenerInfoLista(Int32 cliente)
        {
            return embarcacionObj.Listar_Embarcacion(cliente);
        }
    }
}
