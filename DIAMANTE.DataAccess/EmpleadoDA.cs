﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DIAMANTE.BussinesEntity;
using System.Data;


namespace DIAMANTE.DataAccess
{
    public class EmpleadoDA
    {
        #region No Transaccional
        public EmpleadoBE ObtenerInfo(String usuario, Int32 rol)
        {
            EmpleadoBE empleadoObj = null;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            parameter.Add("@pers_codigo", usuario);
            parameter.Add("@rol_id", rol);

            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_OBTENERINFO_USUARIO", parameter))
            {
                if (dr.Read())
                {
                    empleadoObj = new EmpleadoBE();

                    empleadoObj.Pers_codigo = dr.GetString(dr.GetOrdinal("pers_codigo"));
                    empleadoObj.Empl_nombre = dr.GetString(dr.GetOrdinal("pers_nombre"));
                    empleadoObj.Empl_apellido_paterno = dr.GetString(dr.GetOrdinal("pers_apellido_paterno"));
                    empleadoObj.Empl_apellido_materno = dr.GetString(dr.GetOrdinal("pers_apellido_materno"));
                    empleadoObj.Empl_dni = dr.GetString(dr.GetOrdinal("PERS_DNI"));
                    empleadoObj.Empl_sexo = dr.GetString(dr.GetOrdinal("PERS_SEXO"));
                    empleadoObj.Empl_correo = dr.GetString(dr.GetOrdinal("PERS_CORREO"));
                    empleadoObj.Empl_telefono = dr.GetString(dr.GetOrdinal("PERS_TELEFONO"));
                    empleadoObj.Empl_celular = dr.GetString(dr.GetOrdinal("PERS_CELULAR"));
                    //empleadoObj.Empl_domicilio = dr.GetString(dr.GetOrdinal("PERS_DOMICILIO"));
                    //empleadoObj.Empl_referencia = dr.GetString(dr.GetOrdinal("PERS_REFERENCIA"));
                    empleadoObj.Sede_id = dr.GetInt32(dr.GetOrdinal("sede_id"));
                    empleadoObj.Sede_descripcion = dr.GetString(dr.GetOrdinal("sede_descripcion"));
                    empleadoObj.Rol_id = dr.GetInt32(dr.GetOrdinal("rol_id"));
                    empleadoObj.Rol_nombre = dr.GetString(dr.GetOrdinal("rol_nombre"));
                    empleadoObj.Area_id = dr.GetInt32(dr.GetOrdinal("area_id"));
                    empleadoObj.Area_descripcion = dr.GetString(dr.GetOrdinal("area_descripcion"));
                    //empleadoObj.UNID_ID = dr.GetInt32(dr.GetOrdinal("UNID_ID"));
                    //empleadoObj.UNID_NOMBRE = dr.GetString(dr.GetOrdinal("UNID_NOMBRE"));
                    // DATOS DE ALUMNO
                    //empleadoObj.Carr_descripcion = dr.GetString(dr.GetOrdinal("CARR_DESCRIPCION"));
                    //empleadoObj.Insc_id = dr.GetInt32(dr.GetOrdinal("INSC_ID"));
                    //empleadoObj.Matr_id = dr.GetInt32(dr.GetOrdinal("MATR_ID"));
                }
            }

            return empleadoObj;
        }

        public List<EmpleadoBE> Obtener_Docentes(int sede_id)
        {
            List<EmpleadoBE> lstEmpleadoBE = new List<EmpleadoBE>();
            EmpleadoBE objEmpleadoBE;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@SEDE_ID", sede_id);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_OBTENER_DOCENTES", parameter))
            {
                while (dr.Read())
                {
                    objEmpleadoBE = new EmpleadoBE();
                    objEmpleadoBE.UNID_NOMBRE = dr.GetString(dr.GetOrdinal("PERS_APELLIDO_PATERNO")) + " " + dr.GetString(dr.GetOrdinal("PERS_APELLIDO_MATERNO")) + " " + dr.GetString(dr.GetOrdinal("PERS_NOMBRE"));
                    objEmpleadoBE.Pers_codigo = dr.GetString(dr.GetOrdinal("PERS_CODIGO"));
                    objEmpleadoBE.Empl_apellido_paterno = dr.GetString(dr.GetOrdinal("PERS_APELLIDO_PATERNO"));
                    objEmpleadoBE.Empl_apellido_materno = dr.GetString(dr.GetOrdinal("PERS_APELLIDO_MATERNO"));
                    objEmpleadoBE.Empl_nombre = dr.GetString(dr.GetOrdinal("PERS_NOMBRE"));
                    objEmpleadoBE.Empl_dni = dr.GetString(dr.GetOrdinal("PERS_DNI"));
                    objEmpleadoBE.Empl_telefono = dr.GetString(dr.GetOrdinal("PERS_TELEFONO"));
                    objEmpleadoBE.Empl_correo = dr.GetString(dr.GetOrdinal("PERS_CORREO"));
                    objEmpleadoBE.Empl_referencia = dr.GetString(dr.GetOrdinal("PERS_REFERENCIA"));
                    objEmpleadoBE.Empl_domicilio = dr.GetString(dr.GetOrdinal("PERS_DOMICILIO"));
                    objEmpleadoBE.Empl_celular = dr.GetString(dr.GetOrdinal("PERS_CELULAR"));
                    objEmpleadoBE.Dist_id = dr.GetInt32(dr.GetOrdinal("DIST_ID"));
                    objEmpleadoBE.Empl_sexo = dr.GetString(dr.GetOrdinal("PERS_SEXO"));
                    objEmpleadoBE.Sede_id = dr.GetInt32(dr.GetOrdinal("SEDE_ID"));
                    lstEmpleadoBE.Add(objEmpleadoBE);
                }
            }
            return lstEmpleadoBE;
        }


        // listado de empleados

        public List<EmpleadoBE> ObtenerInfoLista(Int32 SEDE_ID)
        {
            List<EmpleadoBE> optionList = new List<EmpleadoBE>();
            EmpleadoBE empleadoObj;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            parameter.Add("@sede_id", SEDE_ID);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_OBTENERINFOTODOS_EMPLEADOS", parameter))
            {
                if (dr.Read())
                {
                    empleadoObj = new EmpleadoBE();
                    empleadoObj.Empl_apellido_paterno = dr.GetString(dr.GetOrdinal("PERS_APELLIDO_PATERNO"));
                    empleadoObj.Empl_apellido_materno = dr.GetString(dr.GetOrdinal("PERS_APELLIDO_MATERNO"));
                    empleadoObj.Empl_nombre = dr.GetString(dr.GetOrdinal("PERS_NOMBRE"));
                    empleadoObj.Empl_dni = dr.GetString(dr.GetOrdinal("PERS_DNI"));
                    empleadoObj.Empl_telefono = dr.GetString(dr.GetOrdinal("PERS_TELEFONO"));
                    empleadoObj.Empl_correo = dr.GetString(dr.GetOrdinal("PERS_CORREO"));
                    empleadoObj.Empl_referencia = dr.GetString(dr.GetOrdinal("PERS_REFERENCIA"));
                    empleadoObj.Empl_domicilio = dr.GetString(dr.GetOrdinal("PERS_DOMICILIO"));
                    empleadoObj.Empl_celular = dr.GetString(dr.GetOrdinal("PERS_CELULAR"));
                    empleadoObj.Dist_id = dr.GetInt32(dr.GetOrdinal("DIST_ID"));
                    empleadoObj.Empl_sexo = dr.GetString(dr.GetOrdinal("PERS_SEXO"));
                    empleadoObj.Sede_id = dr.GetInt32(dr.GetOrdinal("SEDE_ID"));
                    empleadoObj.Rol_id = dr.GetInt32(dr.GetOrdinal("ROL_ID"));
                    empleadoObj.Rol_nombre = dr.GetString(dr.GetOrdinal("ROL_NOMBRE"));

                    optionList.Add(empleadoObj);
                }
            }

            return optionList;
        }


        #endregion
    }
}
