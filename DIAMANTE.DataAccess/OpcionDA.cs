﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DIAMANTE.BussinesEntity;

namespace DIAMANTE.DataAccess
{
    public class OpcionDA
    {

        #region Transaccional
        public String Insertar(OpcionBE oBe)
        {
            String rpta = string.Empty;

            try
            {
                Dictionary<string, object> parameter = new Dictionary<string, object>();

                parameter.Add("@Opci_padre", oBe.Opci_padre);
                parameter.Add("@Opci_nombre", oBe.Opci_nombre);
                parameter.Add("@Opci_descripcion", oBe.Opci_descripcion);
                parameter.Add("@Opci_ruta", oBe.Opci_ruta);

                rpta = SqlHelper.Instance.ExecuteScalar("SIS_PRC_INSERTAR_OPCION", parameter).ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rpta;
        }

        public String Eliminar(OpcionBE oBe)
        {
            String rpta = string.Empty;

            try
            {
                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("@Opci_id", oBe.Opci_id);
                rpta = SqlHelper.Instance.ExecuteScalar("SIS_PRC_ELIMINAR_OPCION", parameter).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rpta;
        }

        public String Actualizar(OpcionBE oBe)
        {
            String rpta;

            try
            {
                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("@Opci_id", oBe.Opci_id);
                parameter.Add("@Opci_padre", oBe.Opci_padre);
                parameter.Add("@Opci_nombre", oBe.Opci_nombre);
                parameter.Add("@Opci_descripcion", oBe.Opci_descripcion);
                parameter.Add("@Opci_ruta", oBe.Opci_ruta);
                rpta = SqlHelper.Instance.ExecuteScalar("SIS_PRC_ACTUALIZAR_OPCION", parameter).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rpta;
        }
        #endregion

        #region No Transaccional

        public List<OpcionBE> ObtenerxRol(Int32 rol, Int32 padre)
        {

            List<OpcionBE> opcionList = new List<OpcionBE>();
            OpcionBE opcionObj;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            parameter.Add("@rol_id", rol);
            parameter.Add("@opci_padre", padre);

            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_OBTENERXROL_OPCION", parameter))
            {
                while (dr.Read())
                {
                    opcionObj = new OpcionBE();

                    opcionObj.Opci_id = dr.GetInt32(dr.GetOrdinal("opci_id"));
                    opcionObj.Opci_nombre = dr.GetString(dr.GetOrdinal("opci_nombre"));
                    opcionObj.Opci_ruta = dr.GetString(dr.GetOrdinal("opci_ruta"));
                    opcionObj.Opci_Icono = dr.GetString(dr.GetOrdinal("OPCI_ICONO"));

                    opcionList.Add(opcionObj);
                }
            }

            return opcionList;
        }
        //jjjj
        public List<OpcionBE> ObtenerTodos()
        {

            List<OpcionBE> opcionList = new List<OpcionBE>();
            OpcionBE opcionObj;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_OBTENERTODOS_OPCION", parameter))
            {
                while (dr.Read())
                {
                    opcionObj = new OpcionBE();

                    opcionObj.Opci_id = dr.GetInt32(dr.GetOrdinal("opci_id"));
                    opcionObj.Opci_nombre = dr.GetString(dr.GetOrdinal("opci_nombre"));
                    opcionObj.Opci_descripcion = dr.IsDBNull(dr.GetOrdinal("opci_descripcion")) ? string.Empty : dr.GetString(dr.GetOrdinal("opci_descripcion"));
                    opcionObj.Opci_ruta = dr.IsDBNull(dr.GetOrdinal("opci_ruta")) ? string.Empty : dr.GetString(dr.GetOrdinal("opci_ruta"));
                    opcionObj.Opci_padre = dr.GetInt32(dr.GetOrdinal("opci_padre"));
                    opcionList.Add(opcionObj);
                }
            }

            return opcionList;
        }


        public List<OpcionBE> ObtenerOpcionPadre()
        {

            List<OpcionBE> opcionList = new List<OpcionBE>();
            OpcionBE opcionObj;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_OBTENERTODOS_OPCION_PADRE", parameter))
            {
                while (dr.Read())
                {
                    opcionObj = new OpcionBE();

                    opcionObj.Opci_id = dr.GetInt32(dr.GetOrdinal("opci_id"));
                    opcionObj.Opci_nombre = dr.GetString(dr.GetOrdinal("opci_nombre"));
                    opcionList.Add(opcionObj);

                }
            }

            return opcionList;
        }


        public List<OpcionBE> ObtenerxPadre(Int32 padre)
        {

            List<OpcionBE> opcionList = new List<OpcionBE>();
            OpcionBE opcionObj;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@opci_padre", padre);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_OBTENERXPADRE_OPCION", parameter))
            {
                while (dr.Read())
                {
                    opcionObj = new OpcionBE();

                    opcionObj.Opci_id = dr.GetInt32(dr.GetOrdinal("opci_id"));
                    opcionObj.Opci_nombre = dr.GetString(dr.GetOrdinal("opci_nombre"));
                    opcionObj.Opci_padre = dr.GetInt32(dr.GetOrdinal("Opci_padre"));
                    opcionList.Add(opcionObj);
                }
            }

            return opcionList;
        }
        #endregion

        
    }
}
