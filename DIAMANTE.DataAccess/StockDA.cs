﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;
namespace DIAMANTE.DataAccess
{
    public class StockDA
    {
        public List<StockBE> Listar_Stock()
        {
            List<StockBE> lstStock = new List<StockBE>();
            StockBE objStock;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            //parameter.Add("@cod_presentacion", presentacion);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_STOCK_STELENA", parameter))
            {
                while (dr.Read())
                {
                    objStock = new StockBE();
                    objStock.COD_STOCK_STAE = dr.GetInt32(dr.GetOrdinal("COD_STOCK_STAE"));
                    objStock.COD_STOCK_INGRESO = dr.GetInt32(dr.GetOrdinal("COD_STOCK_INGRESO"));
                    objStock.COD_STOCK_INGRESO_DETALLE = dr.GetInt32(dr.GetOrdinal("COD_STOCK_INGRESO_DETALLE"));
                    objStock.FECHA_INFORME = dr.GetDateTime(dr.GetOrdinal("FECHA_INFORME"));
                    objStock.FECHA_INGRESO = dr.GetDateTime(dr.GetOrdinal("FECHA_INGRESO"));
                    objStock.GUIA_NUM = dr.GetString(dr.GetOrdinal("GUIA_NUM"));
                    objStock.REP_NUM = dr.GetString(dr.GetOrdinal("REP_NUM"));
                    objStock.COD_EMBARCACION = dr.GetInt32(dr.GetOrdinal("COD_EMBARCACION"));
                    objStock.NOM_EMBARCACION = dr.GetString(dr.GetOrdinal("NOM_EMBARCACION"));
                    objStock.COD_ESPECIE = dr.GetInt32(dr.GetOrdinal("COD_ESPECIE"));
                    objStock.NOM_ESPECIE = dr.GetString(dr.GetOrdinal("NOM_ESPECIE"));
                    objStock.COD_PRESENTACION = dr.GetInt32(dr.GetOrdinal("COD_PRESENTACION"));
                    objStock.NOM_PRESENTACION = dr.GetString(dr.GetOrdinal("DESCRIPCION"));
                    objStock.NUM_PALLET = dr.GetString(dr.GetOrdinal("NUM_PALLET"));
                    objStock.JPD = dr.GetInt32(dr.GetOrdinal("JPD"));
                    objStock.CANT_BULTOS = dr.GetDecimal(dr.GetOrdinal("CANT_BULTOS"));
                    objStock.PESO_BRUTO = dr.GetDecimal(dr.GetOrdinal("PESO_BRUTO"));
                    objStock.PESO_TARA = dr.GetDecimal(dr.GetOrdinal("PESO_TARA"));
                    objStock.PESO_SUB_NETO = dr.GetDecimal(dr.GetOrdinal("PESO_SUB_NETO"));
                    objStock.PESO_SACO = dr.GetDecimal(dr.GetOrdinal("PESO_SACO"));
                    objStock.PESO_NETO = dr.GetDecimal(dr.GetOrdinal("PESO_NETO"));
                    objStock.LOTE = dr.GetString(dr.GetOrdinal("LOTE"));
                    objStock.OBSERVACION = dr.GetString(dr.GetOrdinal("OBSERVACION"));
                    objStock.COD_OPERACION = dr.GetInt32(dr.GetOrdinal("COD_OPERACION"));
                    objStock.ESTADO = dr.GetInt32(dr.GetOrdinal("ESTADO"));
                    objStock.COD_POSICION = dr.GetInt32(dr.GetOrdinal("COD_POSICION"));
                    objStock.COMP_POSICION = dr.GetString(dr.GetOrdinal("COMP_POSICION"));
                    objStock.SALDO_TOTAL = dr.GetDecimal(dr.GetOrdinal("SALDO_TOTAL"));
                    lstStock.Add(objStock);
                }
            }
            return lstStock;
        }
    }
}
