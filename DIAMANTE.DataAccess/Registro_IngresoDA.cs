﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;
namespace DIAMANTE.DataAccess
{
    public class Registro_IngresoDA
    {
        #region No_Transaccional

        public List<Registro_IngresoBE> Listar_Registro_Ingreso(Int32 TIPO_INGRESO)
        {
            List<Registro_IngresoBE> lstRegistro_Ingreso = new List<Registro_IngresoBE>();

            Registro_IngresoBE objRegistro_Ingreso;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@TIPO_REGISTRO", TIPO_INGRESO);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SE_PRC_LISTAR_REGISTRO_INGRESO", parameter))
            {
                while (dr.Read())
                {
                    objRegistro_Ingreso = new Registro_IngresoBE();
                    objRegistro_Ingreso.ID_REGISTRO = dr.GetInt32(dr.GetOrdinal("ID_REGISTRO"));
                    objRegistro_Ingreso.FECHA_INFORME = dr.GetDateTime(dr.GetOrdinal("FECHA_INFORME"));
                    objRegistro_Ingreso.FECHA_INGRESO = dr.GetDateTime(dr.GetOrdinal("FECHA_INGRESO"));
                    objRegistro_Ingreso.GUIA_NUM = dr.GetString(dr.GetOrdinal("GUIA_NUM"));
                    objRegistro_Ingreso.REP_NUM = dr.GetString(dr.GetOrdinal("REP_NUM"));
                    objRegistro_Ingreso.COD_CLIENTE = dr.GetInt32(dr.GetOrdinal("COD_CLIENTE"));
                    objRegistro_Ingreso.H_INICIO = dr.GetString(dr.GetOrdinal("H_INICIO"));
                    objRegistro_Ingreso.H_SALIDA = dr.GetString(dr.GetOrdinal("H_SALIDA"));
                    objRegistro_Ingreso.PESO_BALANZA = dr.GetDecimal(dr.GetOrdinal("PESO_BALANZA"));
                    objRegistro_Ingreso.JJPP_TT = dr.GetString(dr.GetOrdinal("JJPP_TT"));
                    objRegistro_Ingreso.TCPP_TT = dr.GetString(dr.GetOrdinal("TCPP_TT"));
                    objRegistro_Ingreso.RP_CLIE = dr.GetInt32(dr.GetOrdinal("RP_CLIE"));
                    objRegistro_Ingreso.PLACA = dr.GetString(dr.GetOrdinal("PLACA"));
                    objRegistro_Ingreso.ESTADO = dr.GetInt32(dr.GetOrdinal("ESTADO"));
                    objRegistro_Ingreso.OBSERVACION = dr.GetString(dr.GetOrdinal("OBSERVACION"));
                    objRegistro_Ingreso.TIPO_INGRESO = dr.GetInt32(dr.GetOrdinal("TIPO_INGRESO"));
                    lstRegistro_Ingreso.Add(objRegistro_Ingreso);
                }
            }

            return lstRegistro_Ingreso;
        }


        public List<Registro_IngresoBE> Listar_Registro(Int32 ID)
        {
            List<Registro_IngresoBE> lstRegistro_Ingreso = new List<Registro_IngresoBE>();

            Registro_IngresoBE objRegistro_Ingreso;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ID", ID);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_IMPRIMIR_REGISTRO_INGRESO", parameter))
            {
                while (dr.Read())
                {
                    objRegistro_Ingreso = new Registro_IngresoBE();
                    objRegistro_Ingreso.ID_REGISTRO = dr.GetInt32(dr.GetOrdinal("ID"));
                    objRegistro_Ingreso.NUM_CONTENEDOR = dr.GetString(dr.GetOrdinal("NUM_CONTENEDOR"));
                    objRegistro_Ingreso.FECHA_INFORME = dr.GetDateTime(dr.GetOrdinal("FECHA_INFORME"));
                    objRegistro_Ingreso.FECHA_INGRESO = dr.GetDateTime(dr.GetOrdinal("FECHA_INGRESO"));
                    objRegistro_Ingreso.EMBARCACION = dr.GetString(dr.GetOrdinal("nom_embarcacion"));
                    objRegistro_Ingreso.ESPECIE = dr.GetString(dr.GetOrdinal("nom_especie"));
                    objRegistro_Ingreso.PRESENTACION = dr.GetString(dr.GetOrdinal("DESCRIPCION"));
                    objRegistro_Ingreso.PALLET = dr.GetString(dr.GetOrdinal("NUM_PALLET"));
                    objRegistro_Ingreso.JPD = dr.GetInt32(dr.GetOrdinal("JPD"));
                    objRegistro_Ingreso.PESO_BRUTO = dr.GetDecimal(dr.GetOrdinal("PESO_BRUTO"));
                    objRegistro_Ingreso.PESO_TARA = dr.GetDecimal(dr.GetOrdinal("PESO_TARA"));
                    objRegistro_Ingreso.PESO_SUB_NETO = dr.GetDecimal(dr.GetOrdinal("PESO_SUB_NETO"));
                    objRegistro_Ingreso.PESO_SACO = dr.GetDecimal(dr.GetOrdinal("PESO_SACO"));
                    objRegistro_Ingreso.PESO_NETO = dr.GetDecimal(dr.GetOrdinal("PESO_NETO"));
                    objRegistro_Ingreso.OBSERVACION = dr.GetString(dr.GetOrdinal("OBSERVACION"));
                    objRegistro_Ingreso.DATO_1 = dr.GetString(dr.GetOrdinal("DATO_1"));
                    objRegistro_Ingreso.PERS_APELLIDO_PATERNO = dr.GetString(dr.GetOrdinal("PERS_APELLIDO_PATERNO"));
                    objRegistro_Ingreso.H_INICIO =Convert.ToString(dr.GetOrdinal("H_INICIO"));
                    objRegistro_Ingreso.H_SALIDA = Convert.ToString(dr.GetOrdinal("H_SALIDA"));
                    objRegistro_Ingreso.PESO_BALANZA = dr.GetDecimal(dr.GetOrdinal("PESO_BALANZA"));
                    objRegistro_Ingreso.PLACA = dr.GetString(dr.GetOrdinal("PLACA"));
                    objRegistro_Ingreso.ESTADO = dr.GetInt32(dr.GetOrdinal("ESTADO"));
                    objRegistro_Ingreso.nom_operacion = dr.GetString(dr.GetOrdinal("nom_operacion"));
                    lstRegistro_Ingreso.Add(objRegistro_Ingreso);
                }
            }

            return lstRegistro_Ingreso;
        }

        #endregion

        #region Transaccional

        public String Insertar(Registro_IngresoBE oBe)
        {
            string rpta = string.Empty;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@num_contenedor", oBe.NUM_CONTENEDOR);
            parameter.Add("@PLACA", oBe.PLACA);
            parameter.Add("@presinto", oBe.PRESINTO);
            parameter.Add("@GUIA_NUM", oBe.GUIA_NUM);
            parameter.Add("@TIPO_INGRESO", oBe.TIPO_INGRESO);
            parameter.Add("@RP_CLIE", oBe.RP_CLIE);
            parameter.Add("@COD_CLIENTE", 1);
            parameter.Add("@USUARIO_REGISTRA", oBe.USUARIO_REGISTRA);

            rpta = SqlHelper.Instance.ExecuteScalar("SE_PRC_INSERTAR_REGISTRO_INGRESO", parameter).ToString();

            return rpta;
        }

        public String Completar(Registro_IngresoBE oBe)
        {
            string rpta = string.Empty;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            parameter.Add("@ID_REGISTRO", oBe.ID_REGISTRO);
            parameter.Add("@PESO_BALANZA", oBe.PESO_BALANZA);
            //parameter.Add("@PESO_SACOS", oBe.PESO_SACOS);
            parameter.Add("@OBSERVACION", oBe.OBSERVACION);


            rpta = SqlHelper.Instance.ExecuteScalar("SE_PRC_COMPLETAR_REGISTRO_INGRESO", parameter).ToString();

            return rpta;
        }
        


        #endregion

    }
}
