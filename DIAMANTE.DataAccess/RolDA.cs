﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
using System.Data;

namespace DIAMANTE.DataAccess
{
    public class RolDA
    {
        #region No Transaccional
        public List<RolBE> ObtenerxUsuario(String usuario)
        {

            List<RolBE> rolList = new List<RolBE>();
            RolBE rolObj;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            parameter.Add("@pers_codigo", usuario);

            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_OBTENERXUSUARIO_ROL", parameter))
            {
                while (dr.Read())
                {
                    rolObj = new RolBE();

                    rolObj.Rol_id = dr.GetInt32(dr.GetOrdinal("rol_id"));
                    rolObj.Rol_nombre = dr.GetString(dr.GetOrdinal("rol_nombre"));
                    rolObj.Rol_estado = dr.GetInt32(dr.GetOrdinal("rol_estado"));

                    rolList.Add(rolObj);
                }
            }

            return rolList;
        }

        public List<RolBE> ObtenerLista()
        {

            List<RolBE> rolList = new List<RolBE>();
            RolBE rolObj;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_OBTENER_ROL", parameter))
            {
                while (dr.Read())
                {
                    rolObj = new RolBE();
                    rolObj.Rol_id = dr.GetInt32(dr.GetOrdinal("rol_id"));
                    rolObj.Rol_nombre = dr.GetString(dr.GetOrdinal("rol_nombre"));
                    rolObj.Rol_descripcion = dr.GetString(dr.GetOrdinal("ROL_DESCRIPCION"));
                    rolObj.Rol_estado = dr.GetInt32(dr.GetOrdinal("rol_estado"));
                    rolList.Add(rolObj);
                }
            }

            return rolList;
        }

        public List<UsuarioRolBE> Obtener_Roles_User(String Cod_User)
        {

            List<UsuarioRolBE> rolList = new List<UsuarioRolBE>();
            UsuarioRolBE rolObj = new UsuarioRolBE();
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@PERS_CODIGO", Cod_User);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("TDAC_PRC_OBTENER_ROL_USER", parameter))
            {
                while (dr.Read())
                {
                    rolObj = new UsuarioRolBE();
                    rolObj.Pers_codigo = dr.GetString(dr.GetOrdinal("PERS_CODIGO"));
                    rolObj.Rol_id = dr.GetInt32(dr.GetOrdinal("ROL_ID"));
                    rolObj.Rol_nombre = dr.GetString(dr.GetOrdinal("ROL_NOMBRE"));
                    rolObj.Urol_estado = dr.GetInt32(dr.GetOrdinal("UROL_ESTADO"));
                    rolList.Add(rolObj);
                }
            }

            return rolList;
        }
        public void Actualizar_Rol(RolBE objRol)
        {
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ROL_ID", objRol.Rol_id);
            parameter.Add("@ROL_NOMBRE", objRol.Rol_nombre);
            parameter.Add("@ROL_DESCRIPCION", objRol.Rol_descripcion);
            SqlHelper.Instance.ExecuteScalar("TSIS_PRC_ACTUALIZAR_ROL", parameter);
        }

        public void Limpiar_Rol_Opcion(int rol_id)
        {
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ROL_ID", rol_id);
            SqlHelper.Instance.ExecuteScalar("TSIS_PRC_LIMPIAR_ROL_OPCION", parameter);

        }

        public List<OpcionBE> ObtenerOpcionesxIdRol(int rol_id)
        {
            List<OpcionBE> lstRolBE = new List<OpcionBE>();
            OpcionBE rolObj;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ROL_ID", rol_id);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("TSIS_PRC_OBTENER_xIDROLES_OPCIONES", parameter))
            {
                while (dr.Read())
                {
                    rolObj = new OpcionBE();
                    rolObj.Rol_id = dr.GetInt32(dr.GetOrdinal("rol_id"));
                    rolObj.Opci_id = dr.GetInt32(dr.GetOrdinal("opci_id"));
                    lstRolBE.Add(rolObj);
                }
            }
            return lstRolBE;
        }
        #endregion

        #region Transaccional
        public string RegistrarRol(RolBE objRol)
        {
            String rpta = String.Empty;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ROL_NOMBRE", objRol.Rol_nombre);
            parameter.Add("@ROL_DESCRIPCION", objRol.Rol_descripcion);
            return rpta = SqlHelper.Instance.ExecuteScalar("TSAC_PRC_INSERTAR_ROL", parameter).ToString();
        }

        public string RegistrarRolOpcion(int rol_id, int opci_id)
        {
            String rpta = String.Empty;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ROL_ID", rol_id);
            parameter.Add("@OPCI_ID", opci_id);
            return rpta = SqlHelper.Instance.ExecuteScalar("TSIS_PRC_INSERTAR_ROL_OPCION", parameter).ToString();
        }

        public RolBE ObtenerRolxId(int rol_id)
        {
            RolBE rolObj = new RolBE();
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ROL_ID", rol_id);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("TSIS_PRC_OBTENERxID_ROL", parameter))
            {
                if (dr.Read())
                {
                    rolObj = new RolBE();
                    rolObj.Rol_id = dr.GetInt32(dr.GetOrdinal("rol_id"));
                    rolObj.Rol_nombre = dr.GetString(dr.GetOrdinal("rol_nombre"));
                    rolObj.Rol_descripcion = dr.GetString(dr.GetOrdinal("ROL_DESCRIPCION"));
                    rolObj.Rol_estado = dr.GetInt32(dr.GetOrdinal("rol_estado"));
                }
            }
            return rolObj;
        }
        #endregion

    }
}
