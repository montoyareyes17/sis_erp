﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DIAMANTE.BussinesEntity;


namespace DIAMANTE.DataAccess
{
    public class ProTurnoDA
    {

        public String Insertar(ProTurnoBE oBe)
        {
            String rpta = string.Empty;
            try
            {
                Dictionary<string, object> parameter = new Dictionary<string, object>();

                parameter.Add("@COD_USUARIO", oBe.COD_USUARIO);


                rpta = SqlHelper.Instance.ExecuteScalar("INSERTAR_PRO_TURNO", parameter).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rpta;
        }

        public List<ProTurnoBE> Listar_ProTurno()
        {
            List<ProTurnoBE> lstProTurno = new List<ProTurnoBE>();
            ProTurnoBE objProTurno;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_PRO_TURNO", parameter))
            {
                while (dr.Read())
                {
                    objProTurno = new ProTurnoBE();

                    objProTurno.IDTURNO = dr.GetInt32(dr.GetOrdinal("IDTURNO"));
                    objProTurno.FECHA_INICIO = dr.GetDateTime(dr.GetOrdinal("FECHA_INICIO"));
                    objProTurno.FECHA_CIERRE = dr.GetDateTime(dr.GetOrdinal("FECHA_CIERRE"));
                    objProTurno.USUARIO_NOMBRE = dr.GetString(dr.GetOrdinal("USUARIO"));
                    objProTurno.COD_USUARIO = dr.GetString(dr.GetOrdinal("COD_USUARIO"));
                    objProTurno.ESTADO = dr.GetInt32(dr.GetOrdinal("ESTADO"));

                    lstProTurno.Add(objProTurno);
                }
            }
            return lstProTurno;
        }


    }
}