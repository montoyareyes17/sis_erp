﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DIAMANTE.BussinesEntity;

namespace DIAMANTE.DataAccess
{
    public class Stock_stock_generalDA
    {

        public List<Stock_stock_generalBE> Listar_General()
        {
            List<Stock_stock_generalBE> lstGeneral = new List<Stock_stock_generalBE>();
            Stock_stock_generalBE objGeneral;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("PRC_REPORTE_STOCK_GENERAL", parameter))
            {
                while (dr.Read())
                {
                    objGeneral = new Stock_stock_generalBE();
                    objGeneral.PALLET = dr.GetInt32(dr.GetOrdinal("PALLET"));
                    objGeneral.PESO_NETO = dr.GetDecimal(dr.GetOrdinal("PESO_NETO"));
                    objGeneral.ESPECIE = dr.GetString(dr.GetOrdinal("nom_especie"));

                    lstGeneral.Add(objGeneral);
                }
            }
            return lstGeneral;
        }

    }
}
