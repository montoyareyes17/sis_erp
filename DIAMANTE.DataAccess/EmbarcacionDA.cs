﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;

namespace DIAMANTE.DataAccess
{
    public class EmbarcacionDA
    {
        public List<EmbarcacionBE> Listar_Embarcacion(Int32 cliente)
        {
            List<EmbarcacionBE> lstEmbarcacion = new List<EmbarcacionBE>();
            EmbarcacionBE objEmbarcacion;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@cod_clie", cliente);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_EMBARCACION", parameter))
            {
                while (dr.Read())
                {
                    objEmbarcacion = new EmbarcacionBE ();
                    objEmbarcacion.cod_embarcacion = dr.GetInt32(dr.GetOrdinal("cod_embarcacion"));
                    objEmbarcacion.matr_embarcacion = dr.GetString(dr.GetOrdinal("matr_embarcacion"));
                    objEmbarcacion.nom_embarcacion = dr.GetString(dr.GetOrdinal("nom_embarcacion"));
                    lstEmbarcacion.Add(objEmbarcacion);
                }
            }
            return lstEmbarcacion;
        }
    }
}
