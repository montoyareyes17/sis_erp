﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;
namespace DIAMANTE.DataAccess
{
    public class CalibreDA
    {

        public List<CalibreBE> Listar_Calibre()
        {
            List<CalibreBE> lstCalibre = new List<CalibreBE>();
            CalibreBE objCalibre;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_CALIBRE", parameter))
            {
                while (dr.Read())
                {
                    objCalibre = new CalibreBE();
                    objCalibre.cod_calibre = dr.GetInt32(dr.GetOrdinal("cod_calibre"));
                    objCalibre.nom_calibre = dr.GetString(dr.GetOrdinal("nom_calibre"));
                    lstCalibre.Add(objCalibre);
                }
            }
            return lstCalibre;
        }

    }
}
