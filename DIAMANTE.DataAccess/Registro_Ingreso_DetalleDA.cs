﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;
namespace DIAMANTE.DataAccess
{
    public class Registro_Ingreso_DetalleDA
    {
        #region No_Transaccional
        public List<Registro_Ingreso_DetalleBE> Listar_Registro_Ingreso_Detalle(Int32 ID_REGISTRO)
        {
            List<Registro_Ingreso_DetalleBE> lstRegistro_Ingreso_Detalle = new List<Registro_Ingreso_DetalleBE>();

            Registro_Ingreso_DetalleBE objRegistro_Ingreso;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ID_REGISTRO", ID_REGISTRO);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SE_PRC_LISTAR_DETALLE_REGISTRO", parameter))
            {
                while (dr.Read())
                {
                    objRegistro_Ingreso = new Registro_Ingreso_DetalleBE();
                    objRegistro_Ingreso.ID = dr.GetInt32(dr.GetOrdinal("ID"));
                    objRegistro_Ingreso.ID_REGISTRO = dr.GetInt32(dr.GetOrdinal("ID_REGISTRO"));
                    //objRegistro_Ingreso.REP_NUM = dr.GetString(dr.GetOrdinal("REP_NUM"));
                    objRegistro_Ingreso.COD_EMBARCACION = dr.GetInt32(dr.GetOrdinal("COD_EMBARCACION"));
                    objRegistro_Ingreso.NOM_EMBARCACION = dr.GetString(dr.GetOrdinal("NOM_EMBARCACION"));
                    objRegistro_Ingreso.COD_ESPECIE = dr.GetInt32(dr.GetOrdinal("COD_ESPECIE"));
                    objRegistro_Ingreso.NOM_ESPECIE = dr.GetString(dr.GetOrdinal("NOM_ESPECIE"));
                    objRegistro_Ingreso.COD_PRESENTACION = dr.GetInt32(dr.GetOrdinal("COD_PRESENTACION"));
                    objRegistro_Ingreso.NOM_PRESENTACION = dr.GetString(dr.GetOrdinal("DESCRIPCION"));
                    objRegistro_Ingreso.NUM_PALLET = dr.GetString(dr.GetOrdinal("NUM_PALLET"));
                    objRegistro_Ingreso.JPD = dr.GetInt32(dr.GetOrdinal("JPD"));
                    objRegistro_Ingreso.CANT_BULTOS = dr.GetInt32(dr.GetOrdinal("CANT_BULTOS"));
                    objRegistro_Ingreso.PESO_BRUTO = dr.GetDecimal(dr.GetOrdinal("PESO_BRUTO"));
                    objRegistro_Ingreso.PESO_TARA = dr.GetDecimal(dr.GetOrdinal("PESO_TARA"));
                    //objRegistro_Ingreso.PESO_SUB_NETO = dr.GetDecimal(dr.GetOrdinal("PESO_SUB_NETO"));
                    //objRegistro_Ingreso.PESO_SACO = dr.GetDecimal(dr.GetOrdinal("PESO_SACO"));
                    objRegistro_Ingreso.PESO_NETO = dr.GetDecimal(dr.GetOrdinal("PESO_NETO"));
                    objRegistro_Ingreso.LOTE = dr.GetString(dr.GetOrdinal("LOTE"));
                    objRegistro_Ingreso.OBSERVACION = dr.GetString(dr.GetOrdinal("OBSERVACION"));
                    objRegistro_Ingreso.ESTADO = dr.GetInt32(dr.GetOrdinal("ESTADO"));

                    lstRegistro_Ingreso_Detalle.Add(objRegistro_Ingreso);
                }
            }

            return lstRegistro_Ingreso_Detalle;
        }

        public List<Registro_Ingreso_DetalleBE> Listar_Registro_Detalle(Int32 ID)
        {
            List<Registro_Ingreso_DetalleBE> lstRegistro_Ingreso_Detalle = new List<Registro_Ingreso_DetalleBE>();

            Registro_Ingreso_DetalleBE objRegistro_Ingreso;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ID", ID);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SE_PRC_IMPRIMIR_REGISTRO_INGRESO_DETALLE", parameter))
            {
                while (dr.Read())
                {
                    objRegistro_Ingreso = new Registro_Ingreso_DetalleBE();
                    objRegistro_Ingreso.ID = dr.GetInt32(dr.GetOrdinal("ID"));
                    objRegistro_Ingreso.REP_NUM = dr.GetString(dr.GetOrdinal("REP_NUM"));
                    objRegistro_Ingreso.COD_EMBARCACION = dr.GetInt32(dr.GetOrdinal("COD_EMBARCACION"));
                    objRegistro_Ingreso.NOM_EMBARCACION = dr.GetString(dr.GetOrdinal("NOM_EMBARCACION"));
                    objRegistro_Ingreso.COD_ESPECIE = dr.GetInt32(dr.GetOrdinal("COD_ESPECIE"));
                    objRegistro_Ingreso.NOM_ESPECIE = dr.GetString(dr.GetOrdinal("NOM_ESPECIE"));
                    objRegistro_Ingreso.COD_PRESENTACION = dr.GetInt32(dr.GetOrdinal("COD_PRESENTACION"));
                    objRegistro_Ingreso.NOM_PRESENTACION = dr.GetString(dr.GetOrdinal("DESCRIPCION"));
                    objRegistro_Ingreso.NUM_PALLET = dr.GetString(dr.GetOrdinal("NUM_PALLET"));
                    objRegistro_Ingreso.JPD = dr.GetInt32(dr.GetOrdinal("JPD"));
                    objRegistro_Ingreso.CANT_BULTOS = dr.GetInt32(dr.GetOrdinal("CANT_BULTOS"));
                    objRegistro_Ingreso.PESO_BRUTO = dr.GetDecimal(dr.GetOrdinal("PESO_BRUTO"));
                    objRegistro_Ingreso.PESO_TARA = dr.GetDecimal(dr.GetOrdinal("PESO_TARA"));
                    //objRegistro_Ingreso.PESO_SUB_NETO = dr.GetDecimal(dr.GetOrdinal("PESO_SUB_NETO"));
                    //objRegistro_Ingreso.PESO_SACO = dr.GetDecimal(dr.GetOrdinal("PESO_SACO"));
                    objRegistro_Ingreso.PESO_NETO = dr.GetDecimal(dr.GetOrdinal("PESO_NETO"));
                    objRegistro_Ingreso.LOTE = dr.GetString(dr.GetOrdinal("LOTE"));
                    objRegistro_Ingreso.OBSERVACION = dr.GetString(dr.GetOrdinal("OBSERVACION"));
                    objRegistro_Ingreso.ESTADO = dr.GetInt32(dr.GetOrdinal("ESTADO"));

                    lstRegistro_Ingreso_Detalle.Add(objRegistro_Ingreso);
                }
            }

            return lstRegistro_Ingreso_Detalle;
        }



        #endregion


        #region Transaccional

        public String Insertar(Registro_Ingreso_DetalleBE oBe)
        {
            string rpta = string.Empty;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ID_REGISTRO", oBe.ID_REGISTRO);
            parameter.Add("@EMBARCACION", oBe.COD_EMBARCACION);
            parameter.Add("@ESPECIE", oBe.COD_ESPECIE);
            parameter.Add("@PRESENTACION", oBe.COD_PRESENTACION);
            parameter.Add("@PALLET", oBe.NUM_PALLET);
            parameter.Add("@BRUTO", oBe.PESO_BRUTO);
            parameter.Add("@TARA", oBe.PESO_TARA);

            rpta = SqlHelper.Instance.ExecuteScalar("SE_PRC_REGISTRO_DETALLE_INGRESO", parameter).ToString();
            return rpta;
        }


        #endregion
    }
}
