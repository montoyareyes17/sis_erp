﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;

namespace DIAMANTE.DataAccess
{
    public class RepresentanteClienteDA
    {
        public List<RepresentanteClienteBE> Listar_Representante(Int32 cod_cliente)
        {
            List<RepresentanteClienteBE> lstRepresentante = new List<RepresentanteClienteBE>();
            RepresentanteClienteBE objCliente;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@COD_CLIENTE", cod_cliente);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_REPRESENTANTE_CLIENTE", parameter))
            {
                while (dr.Read())
                {
                    objCliente = new RepresentanteClienteBE();
                    objCliente.DATO_1 = dr.GetString(dr.GetOrdinal("DATO_1"));
                    objCliente.ID = dr.GetInt32(dr.GetOrdinal("id"));
                    lstRepresentante.Add(objCliente);
                }
            }
            return lstRepresentante;
        }
    }
}
