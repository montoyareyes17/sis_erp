﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;
namespace DIAMANTE.DataAccess
{
    public class TipoOperacionDA
    {
        public List<TipoOperacionBE> Listar_Tipo_Operacion(Int32 estado)
        {
            List<TipoOperacionBE> lstTipo_Operacion = new List<TipoOperacionBE>();
            TipoOperacionBE objTipo_Operacion;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@estado", estado);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_TIPO_OPERACION", parameter))
            {
                while (dr.Read())
                {
                    objTipo_Operacion = new TipoOperacionBE();
                    objTipo_Operacion.cod_operacion = dr.GetInt32(dr.GetOrdinal("cod_operacion"));
                    objTipo_Operacion.nom_operacion = dr.GetString(dr.GetOrdinal("nom_operacion"));
                    lstTipo_Operacion.Add(objTipo_Operacion);
                }
            }
            return lstTipo_Operacion;
        }
    }
}
