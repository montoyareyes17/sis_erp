﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;

namespace DIAMANTE.DataAccess
{
    public class ClienteDA
    {
        public List<ClienteBE> Listar_Clientes() {
            List<ClienteBE> lstCliente = new List<ClienteBE>();
            ClienteBE objCliente;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_CLIENTE", parameter))
            {
                while (dr.Read())
                {
                    objCliente = new ClienteBE();
                    objCliente.nom_cliente = dr.GetString(dr.GetOrdinal("nom_cliente"));

                    lstCliente.Add(objCliente);
                }
            }
            return lstCliente;
        }

    }
}
