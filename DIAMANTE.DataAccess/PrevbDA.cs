﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;
namespace DIAMANTE.DataAccess
{
    public class PrevbDA
    {
        #region Transaccional

        public String Insertar(PrevbBE oBe)
        {
            string rpta = string.Empty;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            parameter.Add("@EMBARCACION", oBe.EMBARCACION);
            parameter.Add("@ESPECIE", oBe.ESPECIE);
            parameter.Add("@PRESENTACION", oBe.PRESENTACION);
            parameter.Add("@CALIBRE", oBe.CALIBRE);
            parameter.Add("@LOTE", oBe.LOTE);
            parameter.Add("@CALIDAD", oBe.CALIDAD);
            parameter.Add("@BULTOS", oBe.BULTOS);
            parameter.Add("@FECHA_PRODUCCION", oBe.FECHA_PRODUCCION);
            parameter.Add("@TUNEL", oBe.TUNEL);
            parameter.Add("@CONTRATISTA", oBe.CONTRATISTA);
            parameter.Add("@COD_BARRAS", oBe.COD_BARRAS);
            parameter.Add("@USU_REGISTRA", oBe.USU_REGISTRA);
            parameter.Add("@ESTADO", oBe.ESTADO);
            parameter.Add("@PLAN", oBe.PLAN);
            parameter.Add("@OPERACION", oBe.OPERACION);
            rpta = SqlHelper.Instance.ExecuteScalar("PRC_INSERTAR_PREVB", parameter).ToString();

            return rpta;
        }
        #endregion


        public List<PrevbBE> Listar_Prevb()
        {
            List<PrevbBE> lstPrevb = new List<PrevbBE>();
            PrevbBE objPrevb;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("PRC_LISTAR_PREVB", parameter))
            {
                while (dr.Read())
                {
                    objPrevb = new PrevbBE();

                    objPrevb.ID = dr.GetInt32(dr.GetOrdinal("ID"));
                    objPrevb.EMBARCACION = dr.GetString(dr.GetOrdinal("EMBARCACION"));
                    objPrevb.ESPECIE = dr.GetString(dr.GetOrdinal("ESPECIE"));
                    objPrevb.PRESENTACION = dr.GetString(dr.GetOrdinal("PRESENTACION"));
                    objPrevb.CALIBRE = dr.GetString(dr.GetOrdinal("CALIBRE"));
                    objPrevb.LOTE = dr.GetString(dr.GetOrdinal("LOTE"));
                    objPrevb.CALIDAD = dr.GetString(dr.GetOrdinal("CALIDAD"));
                    objPrevb.BULTOS = dr.GetInt32(dr.GetOrdinal("BULTOS"));
                    objPrevb.FECHA_PRODUCCION = dr.GetDateTime(dr.GetOrdinal("FECHA_PRODUCCION"));
                    objPrevb.TUNEL = dr.GetString(dr.GetOrdinal("TUNEL"));
                    objPrevb.CONTRATISTA = dr.GetString(dr.GetOrdinal("CONTRATISTA"));
                    objPrevb.COD_BARRAS = dr.GetString(dr.GetOrdinal("COD_BARRAS"));
                    objPrevb.FECHA_INGRESO = dr.GetDateTime(dr.GetOrdinal("FECHA_INGRESO"));
                    objPrevb.USU_REGISTRA = dr.GetString(dr.GetOrdinal("USU_REGISTRA"));
                    objPrevb.ESTADO = dr.GetInt32(dr.GetOrdinal("ESTADO"));
                    objPrevb.PLAN = dr.GetString(dr.GetOrdinal("NPLAN"));
                    objPrevb.OPERACION = dr.GetString(dr.GetOrdinal("OPERACION"));

                    lstPrevb.Add(objPrevb);
                }
            }
            return lstPrevb;
        }
    }
}
