﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;
namespace DIAMANTE.DataAccess
{
    public class EspecieDA
    {
        public List<EspecieBE> Listar_Especie(Int32 cliente)
        {
            List<EspecieBE> lstEspecie = new List<EspecieBE>();
            EspecieBE objEspecie;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@cod_clie", cliente);
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_ESPECIE", parameter))
            {
                while (dr.Read())
                {
                    objEspecie = new EspecieBE();
                    objEspecie.cod_especie = dr.GetInt32(dr.GetOrdinal("cod_especie"));
                    objEspecie.nom_especie = dr.GetString(dr.GetOrdinal("nom_especie"));
                    lstEspecie.Add(objEspecie);
                }
            }
            return lstEspecie;
        }
    }
}
