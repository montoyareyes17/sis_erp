﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using DIAMANTE.BussinesEntity;
namespace DIAMANTE.DataAccess
{
    public class PreparacionDA
    {

        #region No_Transaccional

        public List<PreparacionBE> Listar_Preparacion()
        {
            List<PreparacionBE> lstRegistro_Preparacion = new List<PreparacionBE>();

            PreparacionBE objRegistro_Preparacion;

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_PREPARACION", parameter))
            {
                while (dr.Read())
                {
                    objRegistro_Preparacion = new PreparacionBE();
                    objRegistro_Preparacion.COD_PREPARACION = dr.GetInt32(dr.GetOrdinal("COD_PREPARACION"));
                    objRegistro_Preparacion.COD_TIPO_OPERACION = dr.GetInt32(dr.GetOrdinal("COD_TIPO_OPERACION"));
                    objRegistro_Preparacion.NUM_PREPARACION = dr.GetString(dr.GetOrdinal("NUM_PREPARACION"));
                    objRegistro_Preparacion.NOM_OPERACION = dr.GetString(dr.GetOrdinal("nom_operacion"));
                    objRegistro_Preparacion.FECHA_PREPARACION = dr.GetDateTime(dr.GetOrdinal("FECHA_PREPARACION"));
                    objRegistro_Preparacion.ESTADO = dr.GetInt32(dr.GetOrdinal("ESTADO"));

                    lstRegistro_Preparacion.Add(objRegistro_Preparacion);
                }
            }

            return lstRegistro_Preparacion;
        }

        #endregion

        #region Transaccional

        public String Insertar(PreparacionBE oBe)
        {
            string rpta = string.Empty;
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@COD_TIPO_OPERACION", oBe.COD_TIPO_OPERACION);
            parameter.Add("@COD_USUARIO", oBe.COD_USUARIO);
            parameter.Add("@ESTADO", oBe.ESTADO);
            parameter.Add("@NUM_PREPARACION", oBe.NUM_PREPARACION);

            rpta = SqlHelper.Instance.ExecuteScalar("SIS_PRC_INSERTAR_PREPARACION", parameter).ToString();

            return rpta;
        }

        public String Completar(Registro_IngresoBE oBe)
        {
            string rpta = string.Empty;
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            parameter.Add("@ID", oBe.ID_REGISTRO);
            parameter.Add("@PESO_BALANZA", oBe.PESO_BALANZA);
            parameter.Add("@PESO_SACOS", oBe.PESO_SACOS);
            parameter.Add("@OBSERVACION", oBe.OBSERVACION);


            rpta = SqlHelper.Instance.ExecuteScalar("SIS_PRC_COMPLETAR_REGISTRO_INGRESO", parameter).ToString();

            return rpta;
        }



        #endregion

    }
}
