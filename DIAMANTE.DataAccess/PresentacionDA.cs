﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using DIAMANTE.BussinesEntity;

namespace DIAMANTE.DataAccess
{
   public class PresentacionDA
    {



       public List<PresentacionBE> Listar_Presentacion_Catalogo(Int32 cliente)
       {
           List<PresentacionBE> lstPresentacion = new List<PresentacionBE>();
           PresentacionBE objPresentacion;

           Dictionary<string, object> parameter = new Dictionary<string, object>();
           parameter.Add("@cod_cliente", cliente);
           using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_CATALOGO", parameter))
           {
               while (dr.Read())
               {
                   objPresentacion = new PresentacionBE();
                   objPresentacion.cod_PRODUCTO = dr.GetInt32(dr.GetOrdinal("cod_PRODUCTO"));
                   objPresentacion.cod_especie = dr.GetInt32(dr.GetOrdinal("cod_especie"));
                   objPresentacion.id_presentacion = dr.GetString(dr.GetOrdinal("id_presentacion"));
                   objPresentacion.descripcion = dr.GetString(dr.GetOrdinal("Nomenclatura"));
                   objPresentacion.nom_presentacion = dr.GetString(dr.GetOrdinal("DESCRIPCION"));
                   objPresentacion.dur_presentacion = dr.GetInt32(dr.GetOrdinal("dur_presentacion"));
                   objPresentacion.peso_presentacion = dr.GetDecimal(dr.GetOrdinal("peso_presentacion"));
                   objPresentacion.empaque_presentacion = dr.GetString(dr.GetOrdinal("empaque_presentacion"));
                   objPresentacion.estado = dr.GetInt32(dr.GetOrdinal("estado"));
                   objPresentacion.nom_especie = dr.GetString(dr.GetOrdinal("nom_especie"));

                   lstPresentacion.Add(objPresentacion);
               }
           }
           return lstPresentacion;
       }


       public List<PresentacionBE> Listar_Presentacion(Int32 especie)
       {
           List<PresentacionBE> lstPresentacion = new List<PresentacionBE>();
           PresentacionBE objPresentacion;

           Dictionary<string, object> parameter = new Dictionary<string, object>();
           parameter.Add("@cod_especie", especie);
           using (IDataReader dr = SqlHelper.Instance.ExecuteReader("SIS_PRC_LISTAR_PRESENTACION", parameter))
           {
               while (dr.Read())
               {
                   objPresentacion = new PresentacionBE();
                   objPresentacion.cod_PRODUCTO = dr.GetInt32(dr.GetOrdinal("COD_PRODUCTO"));
                   objPresentacion.nom_presentacion = dr.GetString(dr.GetOrdinal("DESCRIPCION"));
                   objPresentacion.nomenclatura = dr.GetString(dr.GetOrdinal("NOMENCLATURA"));
                   objPresentacion.rotulo = dr.GetString(dr.GetOrdinal("ROTULO"));
                   lstPresentacion.Add(objPresentacion);
               }
           }
           return lstPresentacion;
       }
    }
}
