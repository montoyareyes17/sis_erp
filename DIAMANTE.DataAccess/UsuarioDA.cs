﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIAMANTE.BussinesEntity;
namespace DIAMANTE.DataAccess
{
    public class UsuarioDA
    {
        public string ValidarUsuario(UsuarioBE oBe) {

            string resultado = "";
            Dictionary<string, object> parameter = new Dictionary<string, object>();

            parameter.Add("@pers_codigo", oBe.Pers_codigo);
            parameter.Add("@usua_contrasena", oBe.Usua_contrasena);

            resultado = SqlHelper.Instance.ExecuteScalar("SIS_PRC_VALIDAR_USUARIO", parameter).ToString();

            return resultado;
        }
    }
}
