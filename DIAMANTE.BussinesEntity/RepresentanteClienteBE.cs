﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class RepresentanteClienteBE
    {
        public Int32 ID { get; set; }
        public String DATO_1 { get; set; }
        public String cod_cliente { get; set; }
        public String estado { get; set; }
    }
}
