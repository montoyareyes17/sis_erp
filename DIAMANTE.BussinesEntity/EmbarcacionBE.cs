﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class EmbarcacionBE
    {
        public Int32 cod_embarcacion { get; set; }
        public String cod_cliente { get; set; }
        public String matr_embarcacion { get; set; }
        public String nom_embarcacion { get; set; }
        public String estado { get; set; }
    }
}
