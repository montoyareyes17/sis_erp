﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class StockBE
    {
        public Int32 COD_STOCK_STAE { get; set; }
        public Int32 COD_STOCK_INGRESO { get; set; }
        public Int32 COD_STOCK_INGRESO_DETALLE { get; set; }
        public DateTime FECHA_INFORME { get; set; }
        public DateTime FECHA_INGRESO { get; set; }
        public String GUIA_NUM { get; set; }
        public String REP_NUM { get; set; }
        public Int32 COD_EMBARCACION { get; set; }
        public String NOM_EMBARCACION { get; set; }
        public Int32 COD_ESPECIE { get; set; }
        public String NOM_ESPECIE { get; set; }
        public Int32 COD_PRESENTACION { get; set; }
        public String NOM_PRESENTACION { get; set; }
        public String NUM_PALLET { get; set; }
        public Int32 JPD { get; set; }
        public Decimal CANT_BULTOS { get; set; }
        public Decimal PESO_BRUTO { get; set; }
        public Decimal PESO_TARA { get; set; }
        public Decimal PESO_SUB_NETO { get; set; }
        public Decimal PESO_SACO { get; set; }
        public Decimal PESO_NETO { get; set; }
        public String LOTE { get; set; }
        public String OBSERVACION { get; set; }
        public Int32 COD_OPERACION { get; set; }
        public Int32 ESTADO { get; set; }
        public Int32 COD_POSICION { get; set; }
        public String COMP_POSICION { get; set; }
        public Decimal SALDO_TOTAL { get; set; }
    }
}
