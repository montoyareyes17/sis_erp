﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class OpcionBE:RolBE
    {
        public Int32 Opci_id { get; set; }
        public Int32 Opci_padre { get; set; }
        public String Opci_nombre { get; set; }
        public String Opci_descripcion { get; set; }
        public String Opci_ruta { get; set; }
        public Int32 Opci_estado { get; set; }
        public String Opci_Icono { get; set; }
    }
}
