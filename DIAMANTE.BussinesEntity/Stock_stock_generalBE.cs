﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class Stock_stock_generalBE
    {
        public Int32 PALLET { get; set; }
        public Decimal PESO_NETO { get; set; }
        public String ESPECIE { get; set; }
    }
}
