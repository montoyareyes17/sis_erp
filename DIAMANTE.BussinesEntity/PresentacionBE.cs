﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class PresentacionBE
    {
    public Int32 cod_PRODUCTO {get;set;}
	public String nom_presentacion {get;set;}
    public String id_presentacion { get; set; }
    public String descripcion { get; set; }
	public Int32 cod_especie {get;set;}
	public Int32 dur_presentacion {get;set;}
	public Int32 estado {get;set;}
    public Decimal peso_presentacion { get; set; }
    public String empaque_presentacion { get; set; }
    public String nom_especie { get; set; }
    public String nomenclatura { get; set; }
    public String rotulo { get; set; }
    }
}
