﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class Registro_Ingreso_DetalleBE
    {
        public Int32 ID { get; set; }
        public Int32 ID_REGISTRO { get; set; }
        public String REP_NUM { get; set; }
        public Int32 COD_EMBARCACION { get; set; }
        public String NOM_EMBARCACION { get; set; }
        public Int32 COD_ESPECIE { get; set; }
        public String NOM_ESPECIE { get; set; }
        public Int32 COD_PRESENTACION { get; set; }
        public String NOM_PRESENTACION { get; set; }
        public String NUM_PALLET { get; set; }
        public Int32 JPD { get; set; }
        public Int32 CANT_BULTOS { get; set; }
        public Decimal PESO_BRUTO { get; set; }
        public Decimal PESO_TARA { get; set; }
        public Decimal PESO_SUB_NETO { get; set; }
        public Decimal PESO_SACO { get; set; }
        public Decimal PESO_NETO { get; set; }
        public String LOTE { get; set; }
        public String OBSERVACION { get; set; }
        public Int32 ESTADO { get; set; }
    }
}
