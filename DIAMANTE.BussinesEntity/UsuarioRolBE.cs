﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class UsuarioRolBE:RolBE
    {
        #region Propiedades

        public Int32 Urol_id { get; set; }
        public String Pers_codigo { get; set; }
        public Int32 Urol_estado { get; set; }

        #endregion
    }
}
