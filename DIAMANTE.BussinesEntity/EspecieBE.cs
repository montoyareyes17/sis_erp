﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class EspecieBE
    {
        public Int32 cod_especie { get; set; }
        public String cod_cliente { get; set; }
        public String nom_especie { get; set; }
        public String estado { get; set; }
    }
}
