﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class EmpleadoBE
    {
        #region Propiedades

        public String Pers_codigo { get; set; }
        public String Empl_dni { get; set; }
        public String Empl_telefono { get; set; }
        public String Empl_correo { get; set; }
        public String Empl_nombre { get; set; }
        public String Empl_referencia { get; set; }
        public String Empl_apellido_paterno { get; set; }
        public String Empl_apellido_materno { get; set; }
        public String Empl_domicilio { get; set; }
        public String Empl_celular { get; set; }
        public String Empl_usuario_creador { get; set; }
        public DateTime Empl_fecha_creada { get; set; }
        public Int32 Empl_estado { get; set; }
        public Int32 Dist_id { get; set; }
        public String Empl_sexo { get; set; }
        public Int32 Sede_id { get; set; }

        //Rol del empleado
        public Int32 Rol_id { get; set; }
        public String Rol_nombre { get; set; }

        //Area del empleado
        public Int32 Area_id { get; set; }
        public String Area_descripcion { get; set; }

        //Unidad del empleado
        public Int32 UNID_ID { get; set; }
        public String UNID_NOMBRE { get; set; }

        //Sede del empleado
        public String Sede_descripcion { get; set; }

        //datos de alumno
        public Int32 Carr_id { get; set; }
        public String Carr_descripcion { get; set; }
        public Int32 Insc_id { get; set; }
        public Int32 Matr_id { get; set; }

        #endregion
    }
}
