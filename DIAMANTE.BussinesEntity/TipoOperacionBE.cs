﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class TipoOperacionBE
    {
      public Int32 cod_operacion { get; set; }
      public String nom_operacion { get; set; }
      public Int32 estado { get; set; }
    }
}
