﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class Registro_IngresoBE
    {
        public Int32 ID_REGISTRO { get; set; }
        public Int32 TIPO_REGISTRO { get; set; }
        public DateTime FECHA_INFORME { get; set; }
        public DateTime FECHA_INGRESO { get; set; }
        public String GUIA_NUM { get; set; }
        public String REP_NUM { get; set; }
        public Int32 COD_CLIENTE { get; set; }
        public String H_INICIO { get; set; }
        public String H_SALIDA { get; set; }
        public Decimal PESO_BALANZA { get; set; }
        public String JJPP_TT { get; set; }
        public String TCPP_TT { get; set; }
        public Int32 RP_CLIE { get; set; }
        public String PLACA { get; set; }
        public Int32 ESTADO { get; set; }
        public String OBSERVACION { get; set; }
        public Int32 TIPO_INGRESO { get; set; }
        public String NUM_CONTENEDOR { get; set; }
        public String PRESINTO { get; set; }
        public Decimal PESO_SACOS { get; set; }
        public String USUARIO_REGISTRA { get; set; }

        //PARA EL LISTADO DEL DETALLE DEL REPORTE DE INGRESO

        public Decimal PESO_BRUTO { get; set; }
        public Decimal PESO_TARA { get; set; }
        public Decimal PESO_SUB_NETO { get; set; }
        public Decimal PESO_SACO { get; set; }
        public Decimal PESO_NETO { get; set; }
        public String DATO_1 { get; set; }
        public String PERS_APELLIDO_PATERNO { get; set; }
        public String EMBARCACION { get; set; }
        public String ESPECIE { get; set; }
        public String PRESENTACION { get; set; }
        public String PALLET { get; set; }
        public Int32 JPD { get; set; }
        public String nom_operacion { get; set; }
    }
}
