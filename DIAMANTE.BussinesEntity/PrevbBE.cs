﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class PrevbBE
    {
        public Int32 ID { get; set; }
        public String EMBARCACION { get; set; }
        public String ESPECIE { get; set; }
        public String PRESENTACION { get; set; }
        public String CALIBRE { get; set; }
        public String LOTE { get; set; }
        public String CALIDAD { get; set; }
        public Int32 BULTOS { get; set; }
        public DateTime FECHA_PRODUCCION { get; set; }
        public String TUNEL { get; set; }
        public String CONTRATISTA { get; set; }
        public String COD_BARRAS { get; set; }
        public DateTime FECHA_INGRESO { get; set; }
        public String USU_REGISTRA { get; set; }
        public Int32 ESTADO { get; set; }
        public String PLAN { get; set; }
        public String OPERACION { get; set; }
    }
}
