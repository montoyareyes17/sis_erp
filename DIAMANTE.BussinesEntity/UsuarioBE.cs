﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class UsuarioBE
    {
        #region Propiedades

        public String Pers_codigo { get; set; }
        public String Usua_contrasena { get; set; }
        public Int32 Usua_estado { get; set; }

        #endregion
    }
}
