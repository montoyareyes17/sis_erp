﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class PreparacionBE
    {
        public Int32 COD_PREPARACION { get; set; }
        public Int32 COD_TIPO_OPERACION { get; set; }
        public DateTime FECHA_PREPARACION { get; set; }
        public String COD_USUARIO { get; set; }
        public Int32 ESTADO { get; set; }
        public String NUM_PREPARACION { get; set; }
        public String NOM_OPERACION { get; set; }
    }
}
