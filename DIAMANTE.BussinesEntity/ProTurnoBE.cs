﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class ProTurnoBE
    {
        public Int32 IDTURNO { get; set; }
        public DateTime FECHA_INICIO { get; set; }
        public DateTime FECHA_CIERRE { get; set; }
        public String COD_USUARIO { get; set; }
        public Int32 ESTADO { get; set; }

        public String USUARIO_NOMBRE { get; set; }
    }
}
