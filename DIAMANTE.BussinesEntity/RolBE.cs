﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAMANTE.BussinesEntity
{
    public class RolBE
    {
        #region Propiedades

        public Int32 Rol_id { get; set; }
        public String Rol_nombre { get; set; }
        public String Rol_descripcion { get; set; }
        public Int32 Rol_estado { get; set; }

        #endregion
    }
}
