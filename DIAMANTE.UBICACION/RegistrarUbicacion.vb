﻿Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient


Public Class RegistrarUbicacion


    Private Sub RegistrarUbicacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Con = New SqlConnection("Initial Catalog=DIAMANTE; Data Source=localhost;Integrated Security=SSPI;")
        Dim Cmd As New SqlCommand()
        Dim Ds As New DataSet
        Dim Da As New SqlDataAdapter(Cmd)

        'Grilla.DataBind()
        Grilla.Columns.Clear()
        Cmd.CommandText = "PRC_LISTAR_UBICACION_UNION"
        Cmd.CommandTimeout = 5000
        Cmd.CommandType = CommandType.StoredProcedure
        Cmd.Connection = Con

        If Con.State = 1 Then Con.Close()
        Con.Open()
        Da.Fill(Ds, "Migrilla")

        With Grilla
            .DataSource = Ds.Tables(0)

        End With
        Cmd.ExecuteNonQuery()

        Con.Close()

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtData.TextChanged
    End Sub

    Private Sub txtData_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtData.KeyPress
        Dim Con = New SqlConnection("Initial Catalog=DIAMANTE; Data Source=localhost;Integrated Security=SSPI;")
        If e.KeyChar = Chr(13) Then
            Dim command As New SqlCommand("PRC_SIS_ACTUALIZAR_POSICION", Con)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.AddWithValue("@DATO", txtData.Text)
            Try
                Con.Open()
                command.ExecuteNonQuery()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
                Con.Dispose()
                command.Dispose()
            End Try
        End If
    End Sub
End Class