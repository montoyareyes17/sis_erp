﻿var prefix = "#ContentPlaceHolder1_";
var prefix1 = "ContentPlaceHolder1_";

window.onload = function () {
    ListarAperturaTurno();
    $(function () {
        Listar();
        
        $('#tablaRegistro').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })

        $('#tablaAperturaTurno').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
        
        //Date range as a button
        $('#daterange-btn').daterangepicker(
          {
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
          },
          function (start, end) {
              $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
          }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })
        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })

        

    });
    
}

function Tabla() {
    $('#tablaRegistro').dataTable();
    $('#tablaAperturaTurno').dataTable();
}

function Listar() {
    try {
        ObtenerRegistroIngresoEventHandler("", function (result) {
            $('#Registro').html(result);
        });

    } catch (e) {
        throw e;
    }
    return false;
}

function ListarAperturaTurno() {
    try {
        ListarAperturaTurnoEventHandler("", function (result) {
            $('#RegistroAperturaTurno').html(result);
        });

    } catch (e) {
        throw e;
    }
    return false;
}

//LLENA EL COMBO PRESENTACION EN FUNCION A LA ESPECIE
function LlenarComboPresentacion() {
    var data = {
        especie: $(prefix + "ddlEspecie").val()
    };
    var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
    CargarPresentacionEventHandler(arg, function (result) {
        var output = Sys.Serialization.JavaScriptSerializer.deserialize(result);
        if (result.length > 0) {
            $("#divddlPresentacion").html(output.cbo1);
        }
    });
    return false;
}

//LLENA EL COMBO CALIBRE EN FUNCION A LA PRESENTACION
function LlenarComboCalibre() {
    var data = {
        presentacion: $(prefix + "ddlPresentacion").val()
    };
    var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
    CargarCalibreEventHandler(arg, function (result) {
        var output = Sys.Serialization.JavaScriptSerializer.deserialize(result);
        if (result.length > 0) {
            $("#divddlCalibre").html(output.cbo1);
        }
    });
    return false;
}

function Generar_Codigo() {
    var embarcacion = $(prefix + 'ddlEmbarcacion').val();
    var especie = $(prefix + 'ddlEspecie').val();
    var presentacion = $(prefix + 'ddlPresentacion').val();
    var calibre = document.getElementById("txtCalibre").value;
    var lote = document.getElementById("txtLote").value;
    var calidad = $(prefix + 'ddlCalidad').val();
    var bultos = document.getElementById("txtBultos").value;
    var fecha = document.getElementById("datepicker").value;
    //GENERAR NUMERO JULIANO
    Date.prototype.getDOY = function () {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((this - onejan) / 86400000);
    }

    function convertDateToUTC(date) {
        return new Date(date.getUTCFullYear(),
          date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(),
          date.getUTCSeconds());
    }
    dat = String(fecha);
    dia = dat.substring(0, 2);
    mes = dat.substring(3, 5);
    ano = dat.substring(6, 10);

    var today = new Date(dia + ' ' + mes + ", " + ano);
    var a = convertDateToUTC(today);
    var daynum = a.getDOY();

    var1 = (embarcacion + ' 2 ' + presentacion + ' ' + calibre + ' ' + lote + ' ' + calidad + ' ' + bultos + ' ' + daynum + dat.substring(8, 10));
    if (especie == 0 || presentacion == 0) {
        $('#txtResultado').val('Seleccionar la Especie y la presentación');
    } else if (lote == '') {
        $('#txtResultado').val('Ingresar El lote');
        document.getElementById("txtLote").focus();
    } else if (bultos == '') {
        $('#txtResultado').val('Ingresar la cantidad de bultos');
        document.getElementById("txtBultos").focus();
    } else if (fecha == '') {
        $('#txtResultado').val('Seleccione la fecha de producción');
        document.getElementById("datepicker").focus();
    }
    else {
        $('#txtResultado').val(var1);
        Registrar();
    }
}

function Registrar() {
    ///// DATOS PERSONALES /////
    var txtPlan = document.getElementById("txtPlan").value;
    var ddlOperacion = $(prefix + 'ddlOperacion').find('option:selected').text();
    var ddlEmbarcacion = $(prefix + 'ddlEmbarcacion').find('option:selected').text();
    var ddlEspecie = $(prefix + 'ddlEspecie').find('option:selected').text();
    var ddlPresentacion = $(prefix + 'ddlPresentacion').find('option:selected').text();
    var txtCalibre = document.getElementById("txtCalibre").value;
    var txtLote = document.getElementById("txtLote").value;
    var ddlCalidad = $(prefix + 'ddlCalidad').find('option:selected').text();
    var txtBultos = document.getElementById("txtBultos").value;

    dia = document.getElementById("datepicker").value.substring(0, 2);
    mes = document.getElementById("datepicker").value.substring(3, 5);
    ano = document.getElementById("datepicker").value.substring(6, 10);

    var today = new Date(dia + '/' + mes + "/" + ano);


    var txtFechaProd = today;
    var ddlTunel = $(prefix + 'ddlTunel').find('option:selected').text();
    var ddlContratista = $(prefix + 'ddlContratista').find('option:selected').text();
    var txtResultado = document.getElementById("txtResultado").value;

    try {
        var data =
                {
                    PLAN: txtPlan,
                    OPERACION: ddlOperacion,
                    EMBARCACION: ddlEmbarcacion,
                    ESPECIE: ddlEspecie,
                    PRESENTACION: ddlPresentacion,
                    CALIBRE: txtCalibre,
                    LOTE: txtLote,
                    CALIDAD: ddlCalidad,
                    BULTOS: txtBultos,
                    FECHA_PRODUCCION: txtFechaProd,
                    TUNEL: ddlTunel,
                    CONTRATISTA: ddlContratista,
                    COD_BARRAS: txtResultado,
                }
        var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
        RegistrarEventHandler(arg, function (result, context) {
            //$('#modal-default').modal('hide');
            Listar();
        });
        $('#tablaRegistro').dataTable();
    } catch (e) {
        throw e;
    }
}

function AperturaTurno() {
    try {
        var data =
                {
                }
        var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
        AperturarEventHandler(arg, function (result, context) {
            //$('#modal-default').modal('hide');
            ListarAperturaTurno();
            Listar();
        });
        $('#RegistroAperturaTurno').dataTable();
    } catch (e) {
        throw e;
    }
}

function ModalImprimir(IDTURNO) {
    window.open("Reportes/RptCodigo_Barras.aspx?parametro=" + IDTURNO, "_blank");
}