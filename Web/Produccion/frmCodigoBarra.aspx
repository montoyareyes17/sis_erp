﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmCodigoBarra.aspx.cs" Inherits="Web.Produccion.frmProduccion" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Creacion de codigos de Barra
        <small>Pesquera Diamante</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
                <li><a href="#">Produccion</a></li>
                <li class="active">Generar Codigo de Barras</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-4">
                    <!-- /. box -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <form class="form-horizontal">

                                <div class="form-group col-lg-6">
                                    <label class="col-sm-2 control-label">PLAN:</label>
                                    <input type="text" placeholder="N° PLAN" id="txtPlan" class="form-control" />
                                </div>

                                <div class="form-group col-lg-6">
                                    <label class="col-sm-2 control-label">Operacion:</label>
                                    <asp:DropDownList ID="ddlOperacion" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>


                                <div class="form-group col-lg-12">
                                    <label class="col-sm-2 control-label">Embarcacion:</label>
                                    <asp:DropDownList ID="ddlEmbarcacion" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label class="col-sm-2 control-label">Especie:</label>
                                    <asp:DropDownList ID="ddlEspecie" CssClass="form-control" runat="server" onchange="LlenarComboPresentacion()">
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group col-lg-12">
                                    <label class="col-sm-2 control-label">Presentacion:</label>
                                    <div id="divddlPresentacion">
                                        <asp:DropDownList ID="ddlPresentacion" CssClass="form-control" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group col-lg-4">
                                    <label class="col-sm-2 control-label">Calibre:</label>
                                    <input type="text" id="txtCalibre" value="SC" class="form-control">
                                </div>

                                <div class="form-group col-lg-4">
                                    <label class="col-sm-2 control-label">Lote:</label>
                                    <input type="text" placeholder="Lote" id="txtLote" class="form-control">
                                </div>

                                <div class="form-group col-lg-4">
                                    <label class="col-sm-2 control-label">Calidad:</label>
                                    <asp:DropDownList ID="ddlCalidad" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                    <label class="label-validar-m" id="lblddlCalidad"></label>
                                </div>

                                <div class="form-group col-lg-12">
                                    <label class="col-sm-2 control-label">Bultos:</label>
                                    <input type="text" placeholder="Bultos" id="txtBultos" class="form-control" />
                                </div>

                                <div class="form-group col-lg-12">
                                    <div class="form-group">
                                        <label>Date:</label>

                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker" />
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>

                                <div class="form-group col-lg-12">
                                    <label class="col-sm-2 control-label">Tunel:</label>
                                    <asp:DropDownList ID="ddlTunel" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group col-lg-12">
                                    <label class="col-sm-2 control-label">Contratista:</label>
                                    <asp:DropDownList ID="ddlContratista" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>


                                <div class="form-group col-lg-12">
                                    <label class="font-normal control-label col-lg-12">Codigo de Producto:</label>
                                    <input type="text" placeholder="Resultado ..." id="txtResultado" class="form-control input-lg m-b" />
                                </div>


                                <div class="form-group col-lg-12">
                                    <div class="form-group">
                                        <a class="btn btn-success " type="button" href="javascript:Generar_Codigo()"><i class="fa fa-print"></i>&nbsp;&nbsp;<span class="bold">Imprimir</span></a>
                                    </div>
                                    <!-- /.input group -->
                                </div>

                            </form>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <%--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                                            <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>--%>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
                <div class="col-xs-8">
                    <!-- /. box -->
                    <div class="box box-warning direct-chat direct-chat-warning">
                        <div class="box-header">
                            <h3 class="box-title">
                                <a class="btn btn-block btn-success btn-flat pull-right" href="javascript:AperturaTurno()">.:Aperturar Turno:.</a></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <div id="RegistroAperturaTurno">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box box-warning direct-chat direct-chat-warning">
                        <div class="box-header">
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <div id="Registro">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.content -->
        </section>
        <!-- /.content -->
    </div>
    <script src="frmCodigo.js"></script>
</asp:Content>
