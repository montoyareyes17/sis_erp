﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DIAMANTE.WebSite.Util;
using EasyCallback;
using Json;
using DIAMANTE.BussinesEntity;
using DIAMANTE.BussinesLogic;
using System.Collections;

using System.Configuration;
using System.Data.SqlClient;

namespace Web.Produccion
{
    public partial class frmProduccion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //obteniendo el codigo del empleado en session
            EmpleadoBE UsuarioObj = Session["LogUsuario"] as EmpleadoBE;

            if (!IsPostBack)
            {
                CargarMetodos();
            }
        }
        public void CargarMetodos()
        {
            CargarEmbarcacion();
            CargarEspecie();
            CargarCalidad();
            CargarTunel();
            CargarOperacion();
            CargarContratista();

            ddlPresentacion.Enabled = false;
            ddlPresentacion.Items.Insert(0, new ListItem(".:: Seleccione ::.", "0"));
        }
        protected override void OnInit(EventArgs e)
        {
            CallbackManager.Register(ObtenerRegistroIngresoEventHandler);
            CallbackManager.Register(ListarAperturaTurnoEventHandler);

            CallbackManager.Register(CargarPresentacionEventHandler);
            CallbackManager.Register(RegistrarEventHandler);
            CallbackManager.Register(AperturarEventHandler);
            base.OnInit(e);
        }

        public void CargarEmbarcacion()
        {
            Int32 cliente = Convert.ToInt32(3);
            ddlEmbarcacion.DataSource = new EmbarcacionBL().ObtenerInfoLista(cliente);
            ddlEmbarcacion.DataValueField = "matr_embarcacion";
            ddlEmbarcacion.DataTextField = "nom_embarcacion";
            ddlEmbarcacion.DataBind();
            ddlEmbarcacion.Items.Insert(0, new ListItem(".:: Sin Embarcacion ::.", "SE"));
        }

        public void CargarOperacion()
        {
            ddlOperacion.Items.Insert(0, new ListItem(".:: Seleccione ::.", "0"));
            ddlOperacion.Items.Insert(1, new ListItem("PROCESO", "1"));
            ddlOperacion.Items.Insert(2, new ListItem("REPROCESO", "2"));
            ddlOperacion.Items.Insert(3, new ListItem("TRANSFORMACION", "3"));
            ddlOperacion.Items.Insert(4, new ListItem("REEMPAQUE", "4"));
        }

        public void CargarCalidad()
        {
            ddlCalidad.Items.Insert(0, new ListItem(".:: Sin Calidad ::.", "SC"));
            ddlCalidad.Items.Insert(1, new ListItem("A", "A"));
            ddlCalidad.Items.Insert(2, new ListItem("B", "B"));
            ddlCalidad.Items.Insert(3, new ListItem("C", "C"));
            ddlCalidad.Items.Insert(4, new ListItem("D", "D"));
            ddlCalidad.Items.Insert(5, new ListItem("E", "E"));
            ddlCalidad.Items.Insert(6, new ListItem("M", "M"));
        }

        public void CargarTunel()
        {
            ddlTunel.Items.Insert(0, new ListItem(".:: Seleccionar ::.", "0"));
            ddlTunel.Items.Insert(1, new ListItem("Tunel 1", "1"));
            ddlTunel.Items.Insert(2, new ListItem("Tunel 2", "2"));
            ddlTunel.Items.Insert(3, new ListItem("Tunel 3", "3"));
            ddlTunel.Items.Insert(4, new ListItem("Tunel 4", "4"));
            ddlTunel.Items.Insert(5, new ListItem("Tunel 5", "5"));
            ddlTunel.Items.Insert(6, new ListItem("Tunel 6", "6"));
            ddlTunel.Items.Insert(7, new ListItem("Tunel 7", "7"));
            ddlTunel.Items.Insert(8, new ListItem("Tunel 8", "8"));
            ddlTunel.Items.Insert(9, new ListItem("Tunel 9", "9"));
            ddlTunel.Items.Insert(10, new ListItem("Tunel 10", "10"));
        }

        public void CargarContratista()
        {
            ddlContratista.Items.Insert(0, new ListItem(".:: Seleccionar ::.", "0"));
            ddlContratista.Items.Insert(1, new ListItem("Sedelmar", "1"));
            ddlContratista.Items.Insert(2, new ListItem("Profimar", "2"));
            ddlContratista.Items.Insert(3, new ListItem("Medina", "3"));
        }


        public void CargarEspecie()
        {
            Int32 cliente = Convert.ToInt32(3);
            ddlEspecie.DataSource = new EspecieBL().ObtenerInfoLista(cliente);
            ddlEspecie.DataValueField = "cod_especie";
            ddlEspecie.DataTextField = "nom_especie";
            ddlEspecie.DataBind();
            ddlEspecie.Items.Insert(0, new ListItem("Sin Embarcacion", "1"));
        }


        protected String CargarPresentacionEventHandler(String arg)
        {
            try
            {
                Hashtable data = JsonSerializer.FromJson<Hashtable>(arg);
                int especie = int.Parse(data["especie"].ToString());
                if (especie == 0)
                {
                    ddlPresentacion.SelectedIndex = 0;
                }
                else
                {
                    List<PresentacionBE> LstPresentacion = new PresentacionBL().ObtenerInfoLista(especie);
                    ddlPresentacion.Enabled = true;
                    ddlPresentacion.DataSource = LstPresentacion;
                    ddlPresentacion.DataValueField = "ROTULO";
                    ddlPresentacion.DataTextField = "nom_presentacion";
                    ddlPresentacion.DataBind();
                    ddlPresentacion.Items.Insert(0, new ListItem(".:: Seleccione ::.", "0"));
                }

                return JsonSerializer.ToJson(new
                {
                    cbo1 = ddlPresentacion.GetHtmlDdl()
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string AperturarEventHandler(string arg)
        {
            string result = string.Empty;
            //obteniendo la sede y el nombre del usuario creador
            EmpleadoBE UsuarioObj = Session["LogUsuario"] as EmpleadoBE;

            String USUARIO_ID = UsuarioObj.Pers_codigo;

            try
            {
                Hashtable data = JsonSerializer.FromJson<Hashtable>(arg);
                ProTurnoBE oBe = new ProTurnoBE();


                oBe.COD_USUARIO = USUARIO_ID;

                result = new ProTurnoBL().Insertar(oBe);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public string RegistrarEventHandler(string arg)
        {
            string result = string.Empty;
            //obteniendo la sede y el nombre del usuario creador
            EmpleadoBE UsuarioObj = Session["LogUsuario"] as EmpleadoBE;
            String USUARIO_ID = UsuarioObj.Pers_codigo;

            try
            {
                Hashtable data = JsonSerializer.FromJson<Hashtable>(arg);
                PrevbBE oBe = new PrevbBE();
                oBe.PLAN = data["PLAN"].ToString().ToUpper();
                oBe.OPERACION = data["OPERACION"].ToString().ToUpper();
                oBe.EMBARCACION = data["EMBARCACION"].ToString().ToUpper();
                oBe.ESPECIE = data["ESPECIE"].ToString().ToUpper();
                oBe.PRESENTACION = data["PRESENTACION"].ToString().ToUpper();
                oBe.CALIBRE = data["CALIBRE"].ToString().ToUpper();
                oBe.LOTE = data["LOTE"].ToString().ToUpper();
                oBe.CALIDAD = data["CALIDAD"].ToString().ToUpper();
                oBe.BULTOS = Int32.Parse(data["BULTOS"].ToString());
                oBe.FECHA_PRODUCCION = DateTime.Parse(data["FECHA_PRODUCCION"].ToString());
                oBe.TUNEL = data["TUNEL"].ToString().ToUpper();
                oBe.CONTRATISTA = data["CONTRATISTA"].ToString().ToUpper();
                oBe.COD_BARRAS = data["COD_BARRAS"].ToString().ToUpper();
                oBe.USU_REGISTRA = USUARIO_ID;
                oBe.ESTADO = 1;

                result = new PrevbBL().Insertar(oBe);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public string ObtenerRegistroIngresoEventHandler(string arg)
        {
            System.Text.StringBuilder sbRegistro_Ingreso = new System.Text.StringBuilder();
            sbRegistro_Ingreso.Append("<table id=\"tablaRegistro\" class=\"table table-bordered table-striped\">");
            sbRegistro_Ingreso.Append("<thead>");
            sbRegistro_Ingreso.Append("<tr><th>EMBARCACION</th><th>ESPECIE</th><th>PRESENTACION</th><th>CALIBRE</th><th>LOTE</th><th>CALIDAD</th><th>BULTOS</th><th>F. PROD.</th><th>OPERACION</th><th>PLAN</th><th>TUNEL</th><th>CONTRATISTA</th><th>CODIGO</th>");
            sbRegistro_Ingreso.Append("</tr>");

            sbRegistro_Ingreso.Append("</thead>");
            sbRegistro_Ingreso.Append("<tbody>");

            foreach (PrevbBE item in (new PrevbBL().ObtenerInfoLista()))
            {
                sbRegistro_Ingreso.Append("<tr>");
                sbRegistro_Ingreso.Append("<td>" + item.EMBARCACION + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.ESPECIE + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.PRESENTACION + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.CALIBRE + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.LOTE + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.CALIDAD + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.BULTOS + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.FECHA_PRODUCCION + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.OPERACION + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.PLAN + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.TUNEL + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.CONTRATISTA + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.COD_BARRAS + "</td>");

                sbRegistro_Ingreso.Append("</tr>");
            }
            sbRegistro_Ingreso.Append("</tbody>");
            sbRegistro_Ingreso.Append("</table>");
            return sbRegistro_Ingreso.ToString();
        }

        public string ListarAperturaTurnoEventHandler(string arg)
        {
            System.Text.StringBuilder sbRegistro_Ingreso = new System.Text.StringBuilder();
            sbRegistro_Ingreso.Append("<table id=\"tablaAperturaTurno\" class=\"table table-bordered table-striped\">");
            sbRegistro_Ingreso.Append("<thead>");
            sbRegistro_Ingreso.Append("<tr><th>ID</th><th>F. INICIO</th><th>F. CIERRE</th><th>USUARIO</th><th>REPORTE</th>");
            sbRegistro_Ingreso.Append("</tr>");

            sbRegistro_Ingreso.Append("</thead>");
            sbRegistro_Ingreso.Append("<tbody>");

            foreach (ProTurnoBE item in (new ProTurnoBL().ObtenerInfoLista()))
            {
                sbRegistro_Ingreso.Append("<tr>");
                sbRegistro_Ingreso.Append("<td>" + item.IDTURNO + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.FECHA_INICIO + "</td>");

                if (item.ESTADO==1)
                {
                    sbRegistro_Ingreso.Append("<td> TURNO PENDIENTE </td>");
                }
                else
                {
                    sbRegistro_Ingreso.Append("<td>" + item.FECHA_CIERRE +"</td>");
                }

                sbRegistro_Ingreso.Append("<td>" + item.USUARIO_NOMBRE + "</td>");
                //sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalImprimir('" + item.ID + "')\"><i class=\"fa fa-power-off\"></i></a>");
                sbRegistro_Ingreso.Append("<td>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalImprimir('" + item.IDTURNO + "')\"><i class=\"fa fa-print\"></i></a>");
                sbRegistro_Ingreso.Append("</td>");

                sbRegistro_Ingreso.Append("</tr>");
            }
            sbRegistro_Ingreso.Append("</tbody>");
            sbRegistro_Ingreso.Append("</table>");
            return sbRegistro_Ingreso.ToString();
        }

    }
}