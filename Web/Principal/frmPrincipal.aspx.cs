﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DIAMANTE.WebSite.Util;
using EasyCallback;
using Json;
using DIAMANTE.BussinesEntity;
using DIAMANTE.BussinesLogic;
using System.Collections;

namespace Web.Principal
{
    public partial class frmPrincipal1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarDatos();
            }
        }

        protected override void OnInit(EventArgs e)
        {


            base.OnInit(e);
        }

        public void CargarDatos()
        {

            System.Text.StringBuilder sbReporte = new System.Text.StringBuilder();

            //  <span class="progress-text">Add Products to Cart</span>
            //  <span class="progress-number"><b>160</b>/200</span>

            //  <div class="progress sm">
            //    <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
            //  </div>
            //</div>
            Decimal TOTAL_STOCK = 0;
            Decimal PROGRESS_BAR;


            foreach (Stock_stock_generalBE item in (new Stock_stock_generalBL().ObtenerInfoLista()))
            {
                TOTAL_STOCK = item.PESO_NETO + TOTAL_STOCK;
            }



            foreach (Stock_stock_generalBE item in (new Stock_stock_generalBL().ObtenerInfoLista()))
            {
                PROGRESS_BAR = (item.PESO_NETO / TOTAL_STOCK) * 100;
                sbReporte.Append("<div class=\"progress-group\">");
                sbReporte.Append("<span class=\"progress-text\">" + item.ESPECIE + "</span>");
                sbReporte.Append("<span class=\"progress-number\"><b>" + item.PESO_NETO + "</b> / " + TOTAL_STOCK + "</span>");
                sbReporte.Append("<div class=\"progress sm\">");
                sbReporte.Append("<div class=\"progress-bar progress-bar-aqua\" style=\"width:" + Convert.ToInt32(PROGRESS_BAR) + "%\"></div>");
                sbReporte.Append("</div>");
                sbReporte.Append("</div>");
            }

            ltrGeneral.Text = sbReporte.ToString();
        }


    }
}