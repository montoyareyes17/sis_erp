﻿function Ingresar() {
    try {
        var valid = true;

        var user = $('#txtUsuario').val();
        var pass = $('#txtPassword').val();
        var data =
            {
                usuario: user,
                password: pass
            }
        LoginEventHandler(Sys.Serialization.JavaScriptSerializer.serialize(data), function (output) {
            var data = Sys.Serialization.JavaScriptSerializer.deserialize(output);

            if (data.resultado == 'Ok') {
                //location.href = 'frmPrincipal.aspx';
                var data =
                {
                    idUsuario: data.mensaje
                }
                GenerarRolesEventHandler(Sys.Serialization.JavaScriptSerializer.serialize(data), function (result) {
                    $('#roles').html(result);
                });

                $('#modalRoles').modal('show');
            }
            else {
                document.getElementById('mensaje').innerHTML = data.mensaje;
                $("#txtUsuario").val('');
                $("#txtPassword").val('');

                $('#modalMensaje').modal('show');
            }
        });

    } catch (e) {
        throw e;
    }

    return false;
}

function IngresarRol(rol, usuario) {
    try {                                                                                                                                                                                                                                                                                                                                                                                                                                                           
        var data =
            {
                idRol: rol,
                idUsuario: usuario
            }
        ObtenerDatosEventHandler(Sys.Serialization.JavaScriptSerializer.serialize(data), function (result) {
            if (result == 'Ok') {
                location.href = 'Principal/frmPrincipal.aspx';
            }
        });
    } catch (e) {
        throw e;
    }
    return false;
}