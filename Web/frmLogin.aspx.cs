﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using EasyCallback;
using Json;
using System.Collections;
using DIAMANTE.BussinesLogic;
using DIAMANTE.BussinesEntity;


//using EasyCallback;
//using Json;
//using System.Collections;
//using DIAMANTE.BussinesLogic;
//using DIAMANTE.BussinesEntity;


namespace Web
{
    public partial class Index : System.Web.UI.Page
    {

        private const string RolStructura = "<button type=\"button\" class=\"btn btn-primary btn-lg btn-block\"" +
                                            " onclick=\"return IngresarRol({0},'{1}');\">{2}</button>";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ReturnUrl"] != null)
            {
                if (Session["LogUsuario"] != null) FormsAuthentication.RedirectFromLoginPage(Session["LogUsuario"].ToString(), false);
            }
        }


        protected override void OnInit(EventArgs e)
        {
            CallbackManager.Register(LoginEventHandler);
            CallbackManager.Register(GenerarRolesEventHandler);
            CallbackManager.Register(ObtenerDatosEventHandler);
            base.OnInit(e);
        }

        public string LoginEventHandler(string argument)
        {
            try
            {
                Hashtable data = JsonSerializer.FromJson<Hashtable>(argument);

                UsuarioBE oBe = new UsuarioBE()
                {
                    Pers_codigo = data["usuario"].ToString(),
                    Usua_contrasena = data["password"].ToString()
                };

                string result = new UsuarioBL().ValidarUsuario(oBe);

                if (result != "0")
                {
                    //Session["LogUsuario"] = new UsuarioBL().ObtenerInfoUsuarioLogin(result);

                    return JsonSerializer.ToJson(new
                    {
                        resultado = "Ok",
                        mensaje = result
                    });
                }
                else
                {
                    return JsonSerializer.ToJson(new
                    {
                        resultado = "Error",
                        mensaje = "El Usuario y la Contraseña Ingresada No Coinciden."
                    });
                }
            }
            catch (Exception ex)
            {
                return JsonSerializer.ToJson(new
                {
                    resultado = ex.Message,
                    mensaje = ex.Message
                });
            }
        }

        public string GenerarRolesEventHandler(string argument)
        {
            Hashtable data = JsonSerializer.FromJson<Hashtable>(argument);

            System.Text.StringBuilder sbRol = new System.Text.StringBuilder();

            String usuario = data["idUsuario"].ToString();

            foreach (RolBE item in (new RolBL().ObtenerxUsuario(usuario)))
            {
                sbRol.Append(string.Format(RolStructura, item.Rol_id, usuario, item.Rol_nombre));
            }
            return sbRol.ToString();
        }
        public string ObtenerDatosEventHandler(string argument)
        {
            Hashtable data = JsonSerializer.FromJson<Hashtable>(argument);

            String usuario = data["idUsuario"].ToString();
            Int32 rol = Int32.Parse(data["idRol"].ToString());
            Session["LogUsuario"] = new EmpleadoBL().ObtenerInfo(usuario, rol);

            return "Ok";
        }



    }
}