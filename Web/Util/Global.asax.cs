﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace DIAMANTE.WebSite.Util
{
    public class Global : System.Web.HttpApplication
    {
        Int32 contador;
        protected void Application_Start(object sender, EventArgs e)
        {
            contador = 0;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["contador"] = contador++;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Response.Redirect("../frmLogin.aspx");
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Response.Redirect("../frmLogin.aspx");
        }

        protected void Application_End(object sender, EventArgs e)
        {
            Response.Redirect("../frmLogin.aspx");
        }
    }
}