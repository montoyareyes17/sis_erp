﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogin.aspx.cs" Inherits="Web.Index" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html>



<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="assets/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style type="text/css">
        body {
            background-image: url("diamante023.jpg");
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }

        html {
            height: 100%;
        }
    </style>




</head>

<body>
    <div class="login-box">

        <!-- /.login-logo -->
        <div class="login-box-body">
            <img src="assets/logo%20diamante.png" />
            <div class="login-logo">
                <a href="../../index2.html"><b>System</b>CHD</a>
            </div>
            <form runat="server" role="form" class="">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" EnableScriptGlobalization="true"
                    EnableScriptLocalization="true" AsyncPostBackTimeout="600" runat="server">
                </asp:ToolkitScriptManager>

                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Email" id="txtUsuario">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" id="txtPassword">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat" onclick="return Ingresar()">Ingresar</button>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.social-auth-links -->

                <!-- Modal -->
                <div class="modal fade" id="modalMensaje" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Mensaje</h4>
                            </div>
                            <div class="modal-body">
                                <div id="mensaje"></div>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- modal -->








                <!-- Modal Roles -->
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalRoles" class="modal fade">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header" style="background-color: #5cb85c !important;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">DIAMANTE LOGUIN</h4>
                            </div>
                            <div class="modal-body">
                                <h4>Roles</h4>
                                <div id="roles">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal roles-->
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <!-- jQuery 3 -->
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="assets/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '25%' /* optional */
            });
        });
    </script>
    <script src="frmLogin.js"></script>
</body>

</html>
