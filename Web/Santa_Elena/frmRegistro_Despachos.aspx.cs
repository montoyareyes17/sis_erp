﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DIAMANTE.WebSite.Util;
using EasyCallback;
using Json;
using DIAMANTE.BussinesEntity;
using DIAMANTE.BussinesLogic;
using System.Collections;

namespace Web.Santa_Elena
{
    public partial class frmDespachos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected override void OnInit(EventArgs e)
        {
            CallbackManager.Register(ObtenerRegistroDespachosEventHandler);
            base.OnInit(e);
        }

        public string ObtenerRegistroDespachosEventHandler(string arg)
        {
            EmpleadoBE UsuarioObj = Session["LogUsuario"] as EmpleadoBE;
            Int32 SEDE_ID = Convert.ToInt32(UsuarioObj.Sede_id);
            Int32 AREA_ID = (UsuarioObj.Area_id);
            Int32 avance;
            System.Text.StringBuilder sbRegistro_Ingreso = new System.Text.StringBuilder();

            sbRegistro_Ingreso.Append("<table id=\"tablaRegistro\" class=\"table table-bordered table-striped\">");
            sbRegistro_Ingreso.Append("<thead>");
            sbRegistro_Ingreso.Append("<tr><th>Reporte</th><th>Fecha</th><th>Guia</th><th>P. Balanza</th><th>Tipo</th><th>ESTADO</th><th>Opciones</th>");
            sbRegistro_Ingreso.Append("</tr>");

            sbRegistro_Ingreso.Append("</thead>");
            sbRegistro_Ingreso.Append("<tbody>");

            foreach (Registro_IngresoBE item in (new Registro_IngresoBL().ObtenerInfoLista(2)))
            {
                sbRegistro_Ingreso.Append("<tr>");
                sbRegistro_Ingreso.Append("<td>" + item.REP_NUM + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.FECHA_INFORME + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.GUIA_NUM + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.PESO_BALANZA + "</td>");

                if (item.TIPO_INGRESO == 1)
                {
                    sbRegistro_Ingreso.Append("<td> PROCESO </td>");
                }
                else if (item.TIPO_INGRESO == 2)
                {
                    sbRegistro_Ingreso.Append("<td> REPROCESO </td>");
                }
                else if (item.TIPO_INGRESO == 3)
                {
                    sbRegistro_Ingreso.Append("<td> TRANSFORMACION </td>");
                }
                else if (item.TIPO_INGRESO == 4)
                {
                    sbRegistro_Ingreso.Append("<td> CORTE </td>");

                }
                else if (item.TIPO_INGRESO == 5)
                {
                    sbRegistro_Ingreso.Append("<td> COMPRA </td>");
                }
                else if (item.TIPO_INGRESO == 6)
                {
                    sbRegistro_Ingreso.Append("<td> REEMPAQUE </td>");
                }
                else
                {
                    sbRegistro_Ingreso.Append("<td> INVENTARIO </td>");
                }



                //estado= 1 si solo esta creado.
                //2 si se agrego producto yield aun No cierra
                //    3 si ya se cerro


                if (item.ESTADO == 1)
                {
                    avance = 33;
                    sbRegistro_Ingreso.Append("<td><div class=\"slider slider-horizontal\" id=\"red\"><div class=\"slider-track\"><div class=\"slider-selection\" style=\"left: 0%; width: 33.33%;\"></div></div></div></td>");

                    sbRegistro_Ingreso.Append("<td>");
                    sbRegistro_Ingreso.Append("<div class=\"btn-group\">");
                    //sbRegistro_Ingreso.Append("<td class=\"text-center\"><a class=\"btn btn-info btn-xs\" href=\"javascript:ModalImprimir('" + item.REP_NUM + "')\"><i class=\"glyphicon glyphicon-print\"></i></a></td>");

                    //sbOpcion.Append("<li><a href=\"" + Page.ResolveUrl("~" + menu.Opci_ruta) + "\">" + menu.Opci_nombre + "</a></li>");



                    //sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:AgregarDetalle('" + 10 + "')\"><i class=\"fa fa-list-alt fa-plus \"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"" + Page.ResolveUrl("~" + "/Santa_Elena/frmRegistro_Despachos_Detalle.aspx") + "\"><i class=\"fa fa-list-alt fa-plus \"></i></a>");


                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-power-off\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-print\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\"><i class=\"fa fa-close\"></i></a>");
                    sbRegistro_Ingreso.Append("</div>");
                    sbRegistro_Ingreso.Append("</td>");

                }
                else if (item.ESTADO == 2)
                {
                    avance = 66;
                    sbRegistro_Ingreso.Append("<td><div class=\"slider slider-horizontal\" id=\"yellow\"><div class=\"slider-track\"><div class=\"slider-selection\" style=\"left: 0%; width: 66.66%;\"></div></div></div></td>");
                    sbRegistro_Ingreso.Append("<td>");
                    sbRegistro_Ingreso.Append("<div class=\"btn-group\">");
                    //sbRegistro_Ingreso.Append("<td class=\"text-center\"><a class=\"btn btn-info btn-xs\" href=\"javascript:ModalImprimir('" + item.REP_NUM + "')\"><i class=\"glyphicon glyphicon-print\"></i></a></td>");

                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalAgregarItem('" + item.ID_REGISTRO + "','" + item.REP_NUM + "')\"><i class=\"fa fa-list-alt fa-plus \"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalCompletar('" + item.ID_REGISTRO + "')\"><i class=\"fa fa-power-off\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-print\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\"><i class=\"fa fa-close\"></i></a>");
                    sbRegistro_Ingreso.Append("</div>");
                    sbRegistro_Ingreso.Append("</td>");
                }
                else
                {
                    avance = 100;
                    sbRegistro_Ingreso.Append("<td><div class=\"slider slider-horizontal\" id=\"green\"><div class=\"slider-track\"><div class=\"slider-selection\" style=\"left: 0%; width: 100%;\"></div></div></div></td>");
                    sbRegistro_Ingreso.Append("<td>");
                    sbRegistro_Ingreso.Append("<div class=\"btn-group\">");
                    //sbRegistro_Ingreso.Append("<td class=\"text-center\"><a class=\"btn btn-info btn-xs\" href=\"javascript:ModalImprimir('" + item.REP_NUM + "')\"><i class=\"glyphicon glyphicon-print\"></i></a></td>");

                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-list-alt fa-plus \"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-power-off\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalImprimirIngreso('" + item.ID_REGISTRO + "')\"><i class=\"fa fa-print\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\"><i class=\"fa fa-close\"></i></a>");
                    sbRegistro_Ingreso.Append("</div>");
                    sbRegistro_Ingreso.Append("</td>");
                }

                sbRegistro_Ingreso.Append("</tr>");
            }
            sbRegistro_Ingreso.Append("</tbody>");
            sbRegistro_Ingreso.Append("</table>");
            sbRegistro_Ingreso.Append("<img hidden=\"hidden\" src=\"../assets/img/icon.png\" onload=\"Tabla()\">");
            return sbRegistro_Ingreso.ToString();
        }

    }
}