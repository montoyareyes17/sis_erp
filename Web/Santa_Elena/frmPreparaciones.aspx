﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmPreparaciones.aspx.cs" Inherits="Web.Santa_Elena.Stock.frmStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../assets/plugins/bootstrap-slider/slider.css" rel="stylesheet" />


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Registro de Preparaciones
        <small>Santa Elena</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
                <li><a href="#">Santa Elena</a></li>
                <li class="active">Preparaciones</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="col-md-3">
                <a href="javascript:Nuevo()" class="btn btn-primary btn-block margin-bottom">Nueva Preparacion</a>
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Estadistica Mensual</h3>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#"><i class="fa fa-inbox"></i>Pesquero
                  <span class="label label-primary pull-right">12</span></a></li>
                            <li><a href="#"><i class="fa fa-envelope-o"></i>C. Externo</a></li>
                            <li><a href="#"><i class="fa fa-file-text-o"></i>Produccion</a></li>
                            <li><a href="#"><i class="fa fa-filter"></i>Internos <span class="label label-warning pull-right">4</span></a>
                            </li>
                            <li><a href="#"><i class="fa fa-trash-o"></i>Cancelados</a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>

            <div class="col-md-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">DESPACHOS CULMINADOS</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="Preparacion">
                        </div>
                        
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
<%--                        <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                        <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>--%>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog  box-info">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Crear Orden de Salida::</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">

                        <div class="form-group">
                            <label>Nº Operacion:</label>
                            <input type="text" class="form-control" id="txtNumOperacion" placeholder="Contenedor" />
                        </div>

                        <label>Tipo Salida:</label>
                        <asp:DropDownList ID="ddlTIngreso" CssClass="form-control" runat="server" onchange="VerificarTipo()">
                        </asp:DropDownList>
                        <label class="label-validar-m" id="lblddlTIngreso"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                    <%--<button type="button" class="btn btn-primary">Generer Ingreso</button>--%>
                    <a id="btnAceptar"></a>&nbsp;
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modalAgregarStock">
        <div class="modal-dialog modal-lg  box-info">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">STOCK SANTA ELENA:</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <div id="Stock">
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>


    <script src="frmPreparaciones.js"></script>
</asp:Content>
