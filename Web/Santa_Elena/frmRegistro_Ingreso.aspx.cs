﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DIAMANTE.WebSite.Util;
using EasyCallback;
using Json;
using DIAMANTE.BussinesEntity;
using DIAMANTE.BussinesLogic;
using System.Collections;

namespace Web.Santa_Elena
{
    public partial class frmRegistro_Ingreso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarMetodos();
            }

        }

        protected override void OnInit(EventArgs e)
        {
            CallbackManager.Register(ObtenerRegistroIngresoEventHandler);
            CallbackManager.Register(ObtenerRegistroIngresoDetalleEventHandler);
            CallbackManager.Register(RegistrarEventHandler);
            CallbackManager.Register(RegistrarCompletarEventHandler);
            CallbackManager.Register(RegistrarDetalleEventHandler);
            CallbackManager.Register(CargarPresentacionEventHandler);

            base.OnInit(e);
        }

        public void CargarMetodos()
        {
            CargarTipoIngreso();
            CargarRepresentante();
            ddlPresentacion.Enabled = false;
            ddlPresentacion.Items.Insert(0, new ListItem(".:: Seleccione ::.", "0"));
            CargarEmbarcacion();
            CargarEspecie();
        }
        public void CargarEmbarcacion()
        {
            Int32 cliente = Convert.ToInt32(1);
            ddlEmbarcacion.DataSource = new EmbarcacionBL().ObtenerInfoLista(cliente);
            ddlEmbarcacion.DataValueField = "cod_embarcacion";
            ddlEmbarcacion.DataTextField = "nom_embarcacion";
            ddlEmbarcacion.DataBind();
            ddlEmbarcacion.Items.Insert(0, new ListItem(".:: Sin Embarcacion ::.", "1"));
        }
        public void CargarEspecie()
        {
            Int32 cliente = Convert.ToInt32(1);
            ddlEspecie.DataSource = new EspecieBL().ObtenerInfoLista(cliente);
            ddlEspecie.DataValueField = "cod_especie";
            ddlEspecie.DataTextField = "nom_especie";
            ddlEspecie.DataBind();
            ddlEspecie.Items.Insert(0, new ListItem("Sin Embarcacion", "1"));
        }
        protected String CargarPresentacionEventHandler(String arg)
        {
            try
            {
                Hashtable data = JsonSerializer.FromJson<Hashtable>(arg);
                int especie = int.Parse(data["especie"].ToString());
                if (especie == 0)
                {
                    ddlPresentacion.SelectedIndex = 0;
                }
                else
                {
                    List<PresentacionBE> LstPresentacion = new PresentacionBL().ObtenerInfoLista(especie);
                    ddlPresentacion.Enabled = true;
                    ddlPresentacion.DataSource = LstPresentacion;
                    ddlPresentacion.DataValueField = "COD_PRODUCTO";
                    ddlPresentacion.DataTextField = "nom_presentacion";
                    ddlPresentacion.DataBind();
                    ddlPresentacion.Items.Insert(0, new ListItem(".:: Seleccione ::.", "0"));
                }

                return JsonSerializer.ToJson(new
                {
                    cbo1 = ddlPresentacion.GetHtmlDdl()
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CargarTipoIngreso()
        {
            ddlTIngreso.DataSource = new TipoOperacionBL().ObtenerInfoLista(1);
            ddlTIngreso.DataValueField = "cod_operacion";
            ddlTIngreso.DataTextField = "nom_operacion";
            ddlTIngreso.DataBind();
            ddlTIngreso.Items.Insert(0, new ListItem(".:: Seleccione ::.", "1"));
        }
        public void CargarRepresentante()
        {
            Int32 cliente = Convert.ToInt32(1);


            ddlRepresentante.DataSource = new RepresentanteClienteBL().ObtenerInfoLista(cliente);
            ddlRepresentante.DataValueField = "ID";
            ddlRepresentante.DataTextField = "DATO_1";
            ddlRepresentante.DataBind();
            ddlRepresentante.Items.Insert(0, new ListItem(".:: Seleccione ::.", "1"));
        }
        public string ObtenerRegistroIngresoEventHandler(string arg)
        {
            EmpleadoBE UsuarioObj = Session["LogUsuario"] as EmpleadoBE;

            Int32 SEDE_ID = Convert.ToInt32(UsuarioObj.Sede_id);
            Int32 AREA_ID = (UsuarioObj.Area_id);
            Int32 avance;
            System.Text.StringBuilder sbRegistro_Ingreso = new System.Text.StringBuilder();

            sbRegistro_Ingreso.Append("<table id=\"tablaRegistro\" class=\"table table-bordered table-striped\">");
            sbRegistro_Ingreso.Append("<thead>");
            sbRegistro_Ingreso.Append("<tr><th>Reporte</th><th>Fecha</th><th>Guia</th><th>P. Balanza</th><th>Tipo</th><th>ESTADO</th><th>Opciones</th>");
            sbRegistro_Ingreso.Append("</tr>");

            sbRegistro_Ingreso.Append("</thead>");
            sbRegistro_Ingreso.Append("<tbody>");

            foreach (Registro_IngresoBE item in (new Registro_IngresoBL().ObtenerInfoLista(1)))
            {
                sbRegistro_Ingreso.Append("<tr>");
                sbRegistro_Ingreso.Append("<td>" + item.REP_NUM + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.FECHA_INFORME + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.GUIA_NUM + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.PESO_BALANZA + "</td>");

                if (item.TIPO_INGRESO == 1)
                {
                    sbRegistro_Ingreso.Append("<td> PROCESO </td>");
                }
                else if (item.TIPO_INGRESO == 2)
                {
                    sbRegistro_Ingreso.Append("<td> REPROCESO </td>");
                }
                else if (item.TIPO_INGRESO == 3)
                {
                    sbRegistro_Ingreso.Append("<td> TRANSFORMACION </td>");
                }
                else if (item.TIPO_INGRESO == 4)
                {
                    sbRegistro_Ingreso.Append("<td> CORTE </td>");

                }
                else if (item.TIPO_INGRESO == 5)
                {
                    sbRegistro_Ingreso.Append("<td> COMPRA </td>");
                }
                else if (item.TIPO_INGRESO == 6)
                {
                    sbRegistro_Ingreso.Append("<td> REEMPAQUE </td>");
                }
                else
                {
                    sbRegistro_Ingreso.Append("<td> INVENTARIO </td>");
                }



                //estado= 1 si solo esta creado.
                //2 si se agrego producto yield aun No cierra
                //    3 si ya se cerro


                if (item.ESTADO == 1)
                {
                    avance = 33;
                    sbRegistro_Ingreso.Append("<td><div class=\"slider slider-horizontal\" id=\"red\"><div class=\"slider-track\"><div class=\"slider-selection\" style=\"left: 0%; width: 33.33%;\"></div></div></div></td>");

                    sbRegistro_Ingreso.Append("<td>");
                    sbRegistro_Ingreso.Append("<div class=\"btn-group\">");
                    //sbRegistro_Ingreso.Append("<td class=\"text-center\"><a class=\"btn btn-info btn-xs\" href=\"javascript:ModalImprimir('" + item.REP_NUM + "')\"><i class=\"glyphicon glyphicon-print\"></i></a></td>");

                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalAgregarItem('" + item.ID_REGISTRO + "','" + item.REP_NUM + "')\"><i class=\"fa fa-list-alt fa-plus \"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-power-off\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-print\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\"><i class=\"fa fa-close\"></i></a>");
                    sbRegistro_Ingreso.Append("</div>");
                    sbRegistro_Ingreso.Append("</td>");

                }
                else if (item.ESTADO == 2)
                {
                    avance = 66;
                    sbRegistro_Ingreso.Append("<td><div class=\"slider slider-horizontal\" id=\"yellow\"><div class=\"slider-track\"><div class=\"slider-selection\" style=\"left: 0%; width: 66.66%;\"></div></div></div></td>");
                    sbRegistro_Ingreso.Append("<td>");
                    sbRegistro_Ingreso.Append("<div class=\"btn-group\">");
                    //sbRegistro_Ingreso.Append("<td class=\"text-center\"><a class=\"btn btn-info btn-xs\" href=\"javascript:ModalImprimir('" + item.REP_NUM + "')\"><i class=\"glyphicon glyphicon-print\"></i></a></td>");

                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalAgregarItem('" + item.ID_REGISTRO + "','" + item.REP_NUM + "')\"><i class=\"fa fa-list-alt fa-plus \"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalCompletar('" + item.ID_REGISTRO + "')\"><i class=\"fa fa-power-off\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-print\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\"><i class=\"fa fa-close\"></i></a>");
                    sbRegistro_Ingreso.Append("</div>");
                    sbRegistro_Ingreso.Append("</td>");
                }
                else
                {
                    avance = 100;
                    sbRegistro_Ingreso.Append("<td><div class=\"slider slider-horizontal\" id=\"green\"><div class=\"slider-track\"><div class=\"slider-selection\" style=\"left: 0%; width: 100%;\"></div></div></div></td>");
                    sbRegistro_Ingreso.Append("<td>");
                    sbRegistro_Ingreso.Append("<div class=\"btn-group\">");
                    //sbRegistro_Ingreso.Append("<td class=\"text-center\"><a class=\"btn btn-info btn-xs\" href=\"javascript:ModalImprimir('" + item.REP_NUM + "')\"><i class=\"glyphicon glyphicon-print\"></i></a></td>");

                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-list-alt fa-plus \"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default disabled\"><i class=\"fa fa-power-off\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalImprimirIngreso('" + item.ID_REGISTRO + "')\"><i class=\"fa fa-print\"></i></a>");
                    sbRegistro_Ingreso.Append("<a class=\"btn btn-default\"><i class=\"fa fa-close\"></i></a>");
                    sbRegistro_Ingreso.Append("</div>");
                    sbRegistro_Ingreso.Append("</td>");
                }

                sbRegistro_Ingreso.Append("</tr>");
            }
            sbRegistro_Ingreso.Append("</tbody>");
            sbRegistro_Ingreso.Append("</table>");
            sbRegistro_Ingreso.Append("<img hidden=\"hidden\" src=\"../assets/img/icon.png\" onload=\"Tabla()\">");
            return sbRegistro_Ingreso.ToString();
        }

        public string ObtenerRegistroIngresoDetalleEventHandler(string arg)
        {


            System.Text.StringBuilder sbRegistro_Ingreso = new System.Text.StringBuilder();

            sbRegistro_Ingreso.Append("<table id=\"tablaRegistroD\" class=\"table table-bordered table-striped\">");
            sbRegistro_Ingreso.Append("<thead>");
            sbRegistro_Ingreso.Append("<tr><th>Embarcacion</th><th>Especie</th><th>Presentacion</th><th>Pallet</th><th>P. Bruto</th><th>Tara</th><th>Neto</th><th>Opciones</th>");
            sbRegistro_Ingreso.Append("</tr>");

            sbRegistro_Ingreso.Append("</thead>");
            sbRegistro_Ingreso.Append("<tbody>");

            Hashtable data = Json.JsonSerializer.FromJson<Hashtable>(arg);
            Int32 ID_REGISTRO = Convert.ToInt32(data["ID_REGISTRO"].ToString());

            foreach (Registro_Ingreso_DetalleBE item in (new Registro_Ingreso_DetalleBL().ObtenerInfoLista(ID_REGISTRO)))
            {
                sbRegistro_Ingreso.Append("<tr>");
                sbRegistro_Ingreso.Append("<td>" + item.NOM_EMBARCACION + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.NOM_ESPECIE + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.NOM_PRESENTACION + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.NUM_PALLET + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.PESO_BRUTO + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.PESO_TARA + "</td>");
                sbRegistro_Ingreso.Append("<td>" + item.PESO_SUB_NETO + "</td>");



                sbRegistro_Ingreso.Append("<td>");
                sbRegistro_Ingreso.Append("<div class=\"btn-group\">");
                sbRegistro_Ingreso.Append("<a class=\"btn btn-default\" href=\"javascript:ModalImprimir('" + item.ID + "')\"><i class=\"fa fa-barcode\"></i></a>");
                sbRegistro_Ingreso.Append("<a class=\"btn btn-default\"><i class=\"fa fa-trash-o\"></i></a>");
                sbRegistro_Ingreso.Append("</div>");


                sbRegistro_Ingreso.Append("</div>");
                sbRegistro_Ingreso.Append("</div>");
                sbRegistro_Ingreso.Append("</div>");

                sbRegistro_Ingreso.Append("</td>");


                sbRegistro_Ingreso.Append("</tr>");
            }
            sbRegistro_Ingreso.Append("</tbody>");
            sbRegistro_Ingreso.Append("</table>");
            sbRegistro_Ingreso.Append("<img hidden=\"hidden\" src=\"../assets/img/icon.png\" onload=\"Tabla()\">");
            return sbRegistro_Ingreso.ToString();
        }

        public string RegistrarEventHandler(string arg)
        {
            string result = string.Empty;
            //obteniendo la sede y el nombre del usuario creador
            EmpleadoBE UsuarioObj = Session["LogUsuario"] as EmpleadoBE;
            String USUARIO_ID = UsuarioObj.Pers_codigo;

            try
            {
                Hashtable data = JsonSerializer.FromJson<Hashtable>(arg);
                Registro_IngresoBE oBe = new Registro_IngresoBE();

                oBe.NUM_CONTENEDOR = data["contenedor"].ToString().ToUpper();
                oBe.PLACA = data["placa"].ToString().ToUpper();
                oBe.PRESINTO = data["presinto"].ToString().ToUpper();
                oBe.GUIA_NUM = data["guia"].ToString().ToUpper();
                oBe.TIPO_INGRESO = Int32.Parse(data["tipo"].ToString());
                oBe.RP_CLIE = Int32.Parse(data["representante"].ToString());
                oBe.USUARIO_REGISTRA = USUARIO_ID;

                result = new Registro_IngresoBL().Insertar(oBe);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public string RegistrarDetalleEventHandler(string arg)
        {
            string result = string.Empty;
            try
            {
                Hashtable data = JsonSerializer.FromJson<Hashtable>(arg);
                Registro_Ingreso_DetalleBE oBe = new Registro_Ingreso_DetalleBE();

                oBe.ID_REGISTRO = Int32.Parse(data["ID"].ToString());
                oBe.COD_EMBARCACION = Int32.Parse(data["EMBARCACION"].ToString());
                oBe.COD_ESPECIE = Int32.Parse(data["ESPECIE"].ToString());
                oBe.COD_PRESENTACION = Int32.Parse(data["PRESENTACION"].ToString());
                oBe.NUM_PALLET = data["PALLET"].ToString().ToUpper();
                oBe.PESO_BRUTO = Decimal.Parse(data["BRUTO"].ToString());
                oBe.PESO_TARA = Decimal.Parse(data["TARA"].ToString());

                result = new Registro_Ingreso_DetalleBL().Insertar(oBe);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public string RegistrarCompletarEventHandler(string arg)
        {
            string result = string.Empty;

            try
            {
                Hashtable data = JsonSerializer.FromJson<Hashtable>(arg);
                Registro_IngresoBE oBe = new Registro_IngresoBE();

                oBe.ID_REGISTRO = Int32.Parse(data["Id"].ToString().ToUpper());
                oBe.PESO_BALANZA = Decimal.Parse(data["PesoBalanza"].ToString().ToUpper());
                //oBe.PESO_SACOS = Decimal.Parse(data["Sacos"].ToString().ToUpper());
                oBe.OBSERVACION = data["Observacion"].ToString().ToUpper();

                result = new Registro_IngresoBL().Completar(oBe);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}