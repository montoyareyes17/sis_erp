﻿var prefix = "#ContentPlaceHolder1_";
var prefix1 = "ContentPlaceHolder1_";

window.onload = function () {
    Listar();
    $(function () {
        $('#tablaRegistro').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
    })
}

function Tabla() {
    $('#tablaRegistro').dataTable();
}

function Listar() {
    try {
        ObtenerRegistroIngresoEventHandler("", function (result) {
            $('#Registro').html(result);
        });

    } catch (e) {
        throw e;
    }
    return false;
}


function Nuevo() {
    $('#tituloConcepto').html("NUEVO CONCEPTO");
    $('#btnAceptar').html("<a class=\"btn btn-primary\" href=\"javascript:Registrar()\" >Generar Ingreso</a>");
    $('#modal-default').modal('show');
}

function VerificarTipo() {
    var Tipo_Operacion = $(prefix + "ddlTIngreso").val();
    if (Tipo_Operacion==1) {
        $("#txtContenedor").prop("disabled", false);
        $("#txtPlaca").prop("disabled", false);
        $("#txtPresinto").prop("disabled", false);
        $("#txtGuia").prop("disabled", false);
    } else {
        $("#txtContenedor").prop("disabled", true);
        $("#txtPlaca").prop("disabled", true);
        $("#txtPresinto").prop("disabled", true);
        $("#txtGuia").prop("disabled", true);
    }
}

function Registrar() {
    ///// DATOS PERSONALES /////
    var txtContenedor = document.getElementById("txtContenedor").value;
    var txtPlaca = document.getElementById("txtPlaca").value;
    var txtPresinto = document.getElementById("txtPresinto").value;
    var txtGuia = document.getElementById("txtGuia").value;
    var ddlTIngreso = $(prefix + 'ddlTIngreso').val();
    var ddlRepresentante = $(prefix + 'ddlRepresentante').val();
    try {
        var data =
                {
                    ///// DATOS PERSONALES /////
                    contenedor: txtContenedor,
                    placa: txtPlaca,
                    presinto: txtPresinto,
                    guia: txtGuia,
                    tipo: ddlTIngreso,
                    representante: ddlRepresentante,
                }
        var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
        RegistrarEventHandler(arg, function (result, context) {
            $('#modal-default').modal('hide');
            Listar();
        });

        $('#tablaRegistro').dataTable();

    } catch (e) {
        throw e;
    }
}

function ModalAgregarItem(ID_REGISTRO, NUM_REPORTE) {
    var num_reporte = NUM_REPORTE;
    var titulo;
    titulo = "AGREGAR PRODUCTOS: " + num_reporte;
    $('#tituloConcepto').html(titulo);
    $('#btnAgregar').html("<a class=\"fa fa-plus\" href=\"javascript:Registrar_Detalle(" + ID_REGISTRO + ")\" ></a>");
    $('#H1').html(num_reporte);
    $('#ModalAgregarItem').modal('show');
    ListarDetalle(ID_REGISTRO);
}

function ListarDetalle(ID_REGISTRO) {
    var id_registro = ID_REGISTRO;
    var data = {
        ID_REGISTRO: id_registro,
    }
    var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
    ObtenerRegistroIngresoDetalleEventHandler(arg, function (result) {
        $('#RegistroD').html(result);
    })

}

function Registrar_Detalle(ID_REGISTRO) {
    var ddlEmbarcacion = $(prefix + 'ddlEmbarcacion').val();
    var ddlEspecie = $(prefix + 'ddlEspecie').val();
    var ddlPresentacion = $(prefix + 'ddlPresentacion').val();
    var txtPallet = document.getElementById("txtPallet").value;
    var txtBruto = document.getElementById("txtBruto").value;
    var txtTara = document.getElementById("txtTara").value;

    try {
        var data =
                {
                    ID: ID_REGISTRO,
                    EMBARCACION: ddlEmbarcacion,
                    ESPECIE: ddlEspecie,
                    PRESENTACION: ddlPresentacion,
                    PALLET: txtPallet,
                    BRUTO: txtBruto,
                    TARA: txtTara,
                }
        var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
        RegistrarDetalleEventHandler(arg, function (result, context) {
            ListarDetalle(ID_REGISTRO);
        });
    } catch (e) {
        throw e;
    }
}

function ModalCompletar(id) {
    var ID = id;

    $('#guardarCompletar').html("<a class=\"btn btn-primary\" href=\"javascript:Registrar_Completar(" + ID + ")\" >Guardar</a>");
    $('#ModalCompletar').modal('show');
}

function Registrar_Completar(ID) {
    ///// DATOS PERSONALES /////
    var txtPesoBalanza = document.getElementById("txtPesoBalanza").value;
    var txtSacos = document.getElementById("txtSacos").value;
    var txtObservacion = document.getElementById("txtObservacion").value;

    try {
        var data =
                {
                    Id: ID,
                    PesoBalanza: txtPesoBalanza,
                    Sacos: txtSacos,
                    Observacion: txtObservacion,
                }
        var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
        RegistrarCompletarEventHandler(arg, function (result, context) {
            $('#modal-default').modal('hide');
            Listar();
        });

        $('#tablaRegistro').dataTable();

    } catch (e) {
        throw e;
    }
}

function ModalImprimir(ID) {
    window.open("Reportes/ImprimirRotulo.aspx?parametro=" + ID, "imprimir", "width=1000,height=500,scrollbars=yes,resizable=yes")
}

function ModalImprimirIngreso(id) {
    window.open("Reportes/Rpt_Imprimir_Registro_Ingreso.aspx?parametro=" + id, "_blank");
}

//LLENA EL COMBO PROVINCIA Y DISTRITO - DATOS PERSONALES
function LlenarComboPresentacion() {

    var data = {
        especie: $(prefix + "ddlEspecie").val()
    };
    var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
    CargarPresentacionEventHandler(arg, function (result) {
        var output = Sys.Serialization.JavaScriptSerializer.deserialize(result);
        if (result.length > 0) {
            $("#divddlPresentacion").html(output.cbo1);
        }
    });
    return false;
}
/* Sumar dos números. */
function sumar() {

    var total = 0;
    var BRUTO = document.getElementById("txtBruto").value;
    var TARA = document.getElementById("txtTara").value;

    total = parseFloat(BRUTO) - parseFloat(TARA);

    $('#txtNeto').val(total);
}

function tecla(e) {
    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if (key == 13) {
        document.getElementById("txtBulto").focus();
    }
}