﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Rpt_Imprimir_Registro_Ingreso.aspx.cs" Inherits="Web.Santa_Elena.Reportes.Rpt_Imprimir_Registro_Ingreso" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <CR:CrystalReportViewer ID="CrystalReportViewer3" runat="server" AutoDataBind="true" 
                HasCrystalLogo="False" HasToggleGroupTreeButton="False"
                Width="350px" HasToggleParameterPanelButton="False"
                ToolPanelView="None" Height="50px"/>
            <a href="Imprimir_Inscripcion.aspx">Imprimir_Inscripcion.aspx</a>
                EnableDrillDown="False" EnableParameterPrompt="False"
                HasDrillUpButton="False" HasExportButton="true"
                HasGotoPageButton="False" HasPageNavigationButtons="False"
                HasPrintButton="False" HasSearchButton="False"
                HasZoomFactorList="False" />
    </div>
    </form>
</body>
</html>
