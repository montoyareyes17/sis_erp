﻿using DIAMANTE.BussinesEntity;
using DIAMANTE.BussinesLogic;
using DIAMANTE.WebSite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;
namespace Web.Santa_Elena.Reportes
{
    public partial class ImprimirRotulo : System.Web.UI.Page
    {
        ReportDocument rdoc = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            Int32 ID = Int32.Parse(Request["parametro"]);
            List<Registro_Ingreso_DetalleBE> objDetalle= new Registro_Ingreso_DetalleBL().ObtenerInfo(ID);
            rdoc.Load(Server.MapPath("Rpt_Imprimir_Rotulo.rpt"));
            rdoc.SetDataSource(objDetalle);
            CrystalReportViewer3.ReportSource = rdoc;
            CrystalReportViewer3.DataBind();
            rdoc.ExportToHttpResponse
           (CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "Reporte Inscripción");// nombre del pdf
        }
        protected void Page_UnLoad(object sender, EventArgs e)
        {
            if (rdoc != null)
            {
                if (rdoc.IsLoaded)
                {
                    rdoc.Close();
                    rdoc.Dispose();
                }
            }

        }

    }
}