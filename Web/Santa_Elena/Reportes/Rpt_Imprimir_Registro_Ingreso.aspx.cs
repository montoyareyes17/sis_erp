﻿using DIAMANTE.BussinesEntity;
using DIAMANTE.BussinesLogic;
using DIAMANTE.WebSite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;
using Web.DataSet;


namespace Web.Santa_Elena.Reportes
{
    public partial class Rpt_Imprimir_Registro_Ingreso : System.Web.UI.Page
    {
        string conString = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
        SqlCommand cmd = new SqlCommand("SIS_PRC_IMPRIMIR_REGISTRO_INGRESO");
        ReportDocument crystalReport = new ReportDocument();

        private DsImprimirRegistroIngreso GetDatos()
        {
            Int32 ID = Int32.Parse(Request["parametro"]);
            cmd.Parameters.AddWithValue("@ID", ID);
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    sda.SelectCommand = cmd;
                    using (DsImprimirRegistroIngreso dsLista = new DsImprimirRegistroIngreso())
                    {
                        sda.Fill(dsLista, "Dt_Registro_Ingreso");
                        return dsLista;
                    }
                }
            }
        }

        //------- GENERAR REPORTE ------//
        private void Mostrar_Reporte()
        {
            try
            {

                crystalReport.Load(Server.MapPath("Rpt_Ingreso_Reporte.rpt"));
                DsImprimirRegistroIngreso dsLista = GetDatos();
                crystalReport.SetDataSource(dsLista);
                CrystalReportViewer3.ReportSource = crystalReport;
                CrystalReportViewer3.DataBind();
                CrystalReportViewer3.RefreshReport();
                crystalReport.ExportToHttpResponse
                (CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "Reporte_Ingreso");// nombre del pdf              
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //Destruye los Temporales generados por el Crystal Reports.
        private void CloseReports(ReportDocument reportDocument)
        {
            Sections sections = reportDocument.ReportDefinition.Sections;
            foreach (Section section in sections)
            {
                ReportObjects reportObjects = section.ReportObjects;
                foreach (ReportObject reportObject in reportObjects)
                {
                    if (reportObject.Kind == ReportObjectKind.SubreportObject)
                    {
                        SubreportObject subreportObject = (SubreportObject)reportObject;
                        ReportDocument subReportDocument = subreportObject.OpenSubreport(subreportObject.SubreportName);
                        subReportDocument.Close();
                    }
                }
            }
            reportDocument.Close();
        }

        private void Page_Unload(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                CloseReports(crystalReport);
                crystalReport.Dispose();
                CrystalReportViewer3.Dispose();
                CrystalReportViewer3 = null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Mostrar_Reporte();
        }

    }
}