﻿var prefix = "#ContentPlaceHolder1_";
var prefix1 = "ContentPlaceHolder1_";

window.onload = function () {
    Listar();
    $(function () {

        $('#tablaStock').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
    })
}

function Tabla() {
    $('#tablaStock').dataTable();
}

function Listar() {
    try {
        ObtenerPreparacionEventHandler("", function (result) {
            $('#Preparacion').html(result);
        });

    } catch (e) {
        throw e;
    }
    return false;
}

function Listar_2() {
    $(function () {

        $('#tablaStock').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
    })

    try {
        ObtenerInfoListaEventHandler("", function (result) {
            $('#Stock').html(result);
        });

    } catch (e) {
        throw e;
    }
    return false;
}
function Nuevo() {
    $('#tituloConcepto').html("NUEVO CONCEPTO");
    $('#btnAceptar').html("<a class=\"btn btn-primary\" href=\"javascript:Registrar()\" >Generar Ingreso</a>");
    $('#modal-default').modal('show');
}

function AgregarItem(cod_preparacion) {
    CODIGO = cod_preparacion;

    $('#tituloConcepto').html("NUEVO CONCEPTO");
    $('#btnAceptar').html("<a class=\"btn btn-primary\" href=\"javascript:Registrar()\" >Generar Ingreso</a>");
    Listar_2();
    $('#modalAgregarStock').modal('show');
    
}

function Registrar() {
    ///// DATOS PERSONALES /////
    var txtOperacion = document.getElementById("txtNumOperacion").value;
    var ddlTIngreso = $(prefix + 'ddlTIngreso').val();
    try {
        var data =
                {
                    ///// DATOS PERSONALES /////
                    NumeroOperacion: txtOperacion,
                    tipo: ddlTIngreso,
                }

        var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
        RegistrarEventHandler(arg, function (result, context) {
            $('#modal-default').modal('hide');
            Listar();
        });

        $('#tablaRegistro').dataTable();

    } catch (e) {
        throw e;
    }
}