﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DIAMANTE.WebSite.Util;
using EasyCallback;
using Json;
using DIAMANTE.BussinesEntity;
using DIAMANTE.BussinesLogic;
using System.Collections;

namespace Web.Santa_Elena.Stock
{
    public partial class frmStock : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarMetodos();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            CallbackManager.Register(ObtenerPreparacionEventHandler);
            CallbackManager.Register(ObtenerInfoListaEventHandler);
            CallbackManager.Register(RegistrarEventHandler);

            base.OnInit(e);
        }

        public void CargarMetodos()
        {
            CargarTipoIngreso();
        }

        public void CargarTipoIngreso()
        {
            ddlTIngreso.DataSource = new TipoOperacionBL().ObtenerInfoLista(2);
            ddlTIngreso.DataValueField = "cod_operacion";
            ddlTIngreso.DataTextField = "nom_operacion";
            ddlTIngreso.DataBind();
            ddlTIngreso.Items.Insert(0, new ListItem(".:: Seleccione ::.", "1"));
        }

        public string ObtenerPreparacionEventHandler(string arg)
        {
            System.Text.StringBuilder InfoLista = new System.Text.StringBuilder();

            InfoLista.Append("<div class=\"table-responsive\">");
            InfoLista.Append("<table id=\"tablaPreparacion\" class=\"table no-margin\">");
            InfoLista.Append("<thead>");
            InfoLista.Append("<tr><th>Nº ORDEN</th><th>TIPO</th><th>ESTADO</th>");
            InfoLista.Append("</tr>");

            InfoLista.Append("</thead>");
            InfoLista.Append("<tbody>");

            foreach (PreparacionBE item in (new PreparacionBL().ObtenerInfoLista()))
            {
                InfoLista.Append("<tr>");
                InfoLista.Append("<td><a href=\"javascript:AgregarItem(" + item.COD_PREPARACION + ")\">" + item.NUM_PREPARACION + "</a></td>");
                InfoLista.Append("<td>" + item.NOM_OPERACION + "</td>");

                if (item.ESTADO == 1)
                {
                    InfoLista.Append("<td><span class=\"label label-warning\">Preparado</span></td>");
                }
                else if (item.ESTADO == 2)
                {
                    InfoLista.Append("<td><span class=\"label label-success\">Despachado</span></td>");
                }
                else if (item.ESTADO == 3)
                {
                    InfoLista.Append("<td><span class=\"label label-danger\">Canclado</span></td>");
                }
                InfoLista.Append("</tr>");
            }
            InfoLista.Append("</tbody>");
            InfoLista.Append("</table>");
            InfoLista.Append("</div>");
            InfoLista.Append("<img hidden=\"hidden\" src=\"../assets/img/icon.png\" onload=\"Tabla()\">");
            return InfoLista.ToString();
        }

        public string ObtenerInfoListaEventHandler(string arg)
        {
            System.Text.StringBuilder InfoLista = new System.Text.StringBuilder();

            InfoLista.Append("<table id=\"tablaStock\" class=\"table table-bordered table-striped\">");
            InfoLista.Append("<thead>");
            InfoLista.Append("<tr><th>F. INGRESO</th><th>ORIGEN</th><th>ESPECIE</th><th>PRESENTACION</th><th>PALLET</th><th>P. SUB NETO</th><th>SACO</th><th>P. NETO</th><th>SALDO TOT.</th><th>ASIGNAR</th>");
            InfoLista.Append("</tr>");

            InfoLista.Append("</thead>");
            InfoLista.Append("<tbody>");

            foreach (StockBE item in (new StockBL().ObtenerInfoLista()))
            {
                InfoLista.Append("<tr>");

                //InfoLista.Append("<td>" + item.COD_STOCK_STAE + "</td>");
                InfoLista.Append("<td>" + item.FECHA_INGRESO + "</td>");
                //InfoLista.Append("<td>" + item.GUIA_NUM + "</td>");
                InfoLista.Append("<td>" + item.NOM_EMBARCACION + "</td>");
                InfoLista.Append("<td>" + item.NOM_ESPECIE + "</td>");
                InfoLista.Append("<td>" + item.NOM_PRESENTACION + "</td>");
                InfoLista.Append("<td>" + item.NUM_PALLET + "</td>");
                //InfoLista.Append("<td>" + item.JPD + "</td>");
                InfoLista.Append("<td>" + item.PESO_SUB_NETO + "</td>");
                InfoLista.Append("<td>" + item.PESO_SACO + "</td>");
                InfoLista.Append("<td>" + item.PESO_NETO + "</td>");
                //InfoLista.Append("<td>" + item.COMP_POSICION + "</td>");
                InfoLista.Append("<td>" + item.SALDO_TOTAL + "</td>");
                //InfoLista.Append("<td>" + item.OBSERVACION + "</td>");

                InfoLista.Append("<td>");
                InfoLista.Append("<div class=\"btn-group\">");
                //sbRegistro_Ingreso.Append("<td class=\"text-center\"><a class=\"btn btn-info btn-xs\" href=\"javascript:ModalImprimir('" + item.REP_NUM + "')\"><i class=\"glyphicon glyphicon-print\"></i></a></td>");

                InfoLista.Append("<a class=\"btn btn-default\" href=\"javascript:ModalAgregarItem()\"><i class=\"fa fa-list-alt fa-ship \"></i></a>");
                InfoLista.Append("</div>");
                InfoLista.Append("</td>");


                InfoLista.Append("</tr>");
            }
            InfoLista.Append("</tbody>");
            InfoLista.Append("</table>");
            InfoLista.Append("<img hidden=\"hidden\" src=\"../assets/img/icon.png\" onload=\"Tabla()\">");
            return InfoLista.ToString();
        }

        public string RegistrarEventHandler(string arg)
        {
            string result = string.Empty;
            //obteniendo la sede y el nombre del usuario creador
            EmpleadoBE UsuarioObj = Session["LogUsuario"] as EmpleadoBE;
            String USUARIO_ID = UsuarioObj.Pers_codigo;

            try
            {
                Hashtable data = JsonSerializer.FromJson<Hashtable>(arg);
                PreparacionBE oBe = new PreparacionBE();

                oBe.COD_TIPO_OPERACION = Int32.Parse(data["tipo"].ToString());
                oBe.NUM_PREPARACION = data["NumeroOperacion"].ToString().ToUpper();
                oBe.COD_USUARIO = USUARIO_ID;
                oBe.ESTADO = 1;

                result = new PreparacionBL().Insertar(oBe);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}