﻿var prefix = "#ContentPlaceHolder1_";
var prefix1 = "ContentPlaceHolder1_";


window.onload = function () {
    $(function () {
        $('#tablaStock').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
    })
}

function Tabla() {
    $('#tablaStock').dataTable();
}

function Listar() {
    var pallet = document.getElementById("txtPallet").value;
    alert(pallet);
    try {
        var data =
               {
                   PALLET: pallet,
               }
        var arg = Sys.Serialization.JavaScriptSerializer.serialize(data);
        ObtenerInfoListaEventHandler("", function (result) {
            $('#Despacho').html(result);
        });

    } catch (e) {
        throw e;
    }
    return false;
}

function pulsar(e) {
    if (e.keyCode === 13 && !e.shiftKey) {
        e.preventDefault();
        Listar();
    }
}