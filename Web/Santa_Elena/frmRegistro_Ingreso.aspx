﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmRegistro_Ingreso.aspx.cs" Inherits="Web.Santa_Elena.frmRegistro_Ingreso" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../assets/plugins/bootstrap-slider/slider.css" rel="stylesheet" />
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Registro de Ingreso
        <small>Santa Elena</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
                <li><a href="#">Santa Elena</a></li>
                <li class="active">Registro de Ingreso</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                <a class="btn btn-block btn-success btn-flat pull-right" href="javascript:Nuevo()">Nuevo Ingreso</a></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <div id="Registro">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

        <div class="modal fade" id="modal-default">
            <div class="modal-dialog  box-info">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Registro de Ingresos:</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tipo Ingreso:</label>
                            <asp:DropDownList ID="ddlTIngreso" CssClass="form-control" runat="server" onchange="VerificarTipo()">
                            </asp:DropDownList>
                            <label class="label-validar-m" id="lblddlTIngreso"></label>
                        </div>

                        <div class="form-group">
                            <label>Contenedor:</label>
                            <input type="text" class="form-control" id="txtContenedor" placeholder="Contenedor" />
                        </div>

                        <div class="form-group">
                            <label>Placa:</label>
                            <input type="text" class="form-control" id="txtPlaca" placeholder="Placa" />
                        </div>
                        <div class="form-group">
                            <label>Presinto:</label>
                            <input type="text" class="form-control" id="txtPresinto" placeholder="Presinto" />
                        </div>

                        <div class="form-group">
                            <label>Guia:</label>
                            <input type="text" class="form-control" id="txtGuia" placeholder="Guia" />
                        </div>
                        <div class="form-group">
                            <label>Representante:</label>
                            <asp:DropDownList ID="ddlRepresentante" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                            <label class="label-validar-m" id="lblddlRepresentante"></label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                        <%--<button type="button" class="btn btn-primary">Generer Ingreso</button>--%>
                        <a id="btnAceptar"></a>&nbsp;
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="ModalAgregarItem">
            <div class="modal-dialog modal-lg box-info">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="tituloConcepto"></h4>
                        <h4 class="modal-title" id="H1" hidden="hidden"></h4>
                    </div>
                    <div class="modal-body">

                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="form-group col-md-2">
                                    <label>Embarcacion</label>
                                    <asp:DropDownList ID="ddlEmbarcacion" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                    <label class="label-validar-m" id="lblddlEmbarcacion"></label>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Especie</label>
                                    <asp:DropDownList ID="ddlEspecie" CssClass="form-control" runat="server" onchange="LlenarComboPresentacion()">
                                    </asp:DropDownList>
                                    <label class="label-validar-m" id="lblddlEspecie"></label>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Presentacion</label>
                                    <div id="divddlPresentacion">
                                        <asp:DropDownList ID="ddlPresentacion" CssClass="form-control" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <label class="label-validar-m" id="lblddlPresentacion"></label>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Pallet</label>
                                    <input type="text" class="form-control" id="txtPallet" placeholder="Pallet" />
                                    <%--onkeydown="tecla(event);" />--%>
                                </div>
                                <div class="form-group col-md-1">
                                    <label>P. Bruto</label>
                                    <input type="text" id="txtBruto" class="form-control" value="0" onkeyup="sumar();" />
                                </div>
                                <div class="form-group col-md-1">
                                    <label>Tara</label>
                                    <input type="text" class="form-control" id="txtTara" value="0" onkeyup="sumar();" />
                                </div>
                                <div class="form-group col-md-1">
                                    <label>Neto</label>
                                    <input type="text" disabled="disabled" class="form-control" id="txtNeto" placeholder="Neto" />
                                </div>

                                <div class="form-group col-md-1">
                                    <a id="btnAgregar"></a>
                                </div>
                            </div>
                        </div>

                        <div class="box box-warning">
                            <div class="box-header with-border">
                                <div id="RegistroD"></div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                            <%--<button type="button" class="btn btn-primary">Generer Ingreso</button>--%>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>


        </div>


        <div class="modal fade" id="ModalCompletar">
            <div class="modal-dialog">

                <div class="modal-content box box-info">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Cerrar Recepcion:</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group col-md-6">
                            <label>PESO NETO BALANZA:</label>
                            <input type="text" id="txtPesoBalanza" class="form-control" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>PESO NETO SACOS</label>
                            <input type="text" id="txtSacos" class="form-control" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>OBSERVACION</label>
                            <textarea id="txtObservacion" class="form-control" rows="3" placeholder="Obser ..."></textarea>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                            <a id="guardarCompletar"></a>
                            <%--<button type="button" class="btn btn-primary">Generer Ingreso</button>--%>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>

    </div>
    <script src="frmRegistro_Ingreso.js"></script>
</asp:Content>
