﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmRegistro_Despachos.aspx.cs" Inherits="Web.Santa_Elena.frmDespachos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../assets/plugins/bootstrap-slider/slider.css" rel="stylesheet" />
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Registro de Despachos
        <small>Santa Elena</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
                <li><a href="#">Santa Elena</a></li>
                <li class="active">Registro de Despachos</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                <a class="btn btn-block btn-success btn-flat pull-right" href="javascript:Nuevo()">Nueva Salida</a></h3>
                        </div>
                        <hr />
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <div id="Registro">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script src="frmRegistro_Despachos.js"></script>
</asp:Content>