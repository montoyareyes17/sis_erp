﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using EasyCallback;
using Json;
using System.Collections;
using DIAMANTE.BussinesLogic;
using DIAMANTE.BussinesEntity;

namespace DIAMANTE.Web.MasterPage
{
    public partial class frmMasterPage : System.Web.UI.MasterPage
    {
        public String SERVIDOR = "";
        String id_usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if (!IsPostBack)
            {
                if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    Session.Abandon();
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                }

                if (Session["LogUsuario"] != null)
                {
                    EmpleadoBE UsuarioObj = Session["LogUsuario"] as EmpleadoBE;

                    lblUsuario.Text = UsuarioObj.Empl_nombre + "  " + UsuarioObj.Empl_apellido_paterno;
                    lblUsuarioB.Text = UsuarioObj.Empl_nombre;
                    lblCargo.Text = UsuarioObj.Rol_nombre + " - " + UsuarioObj.Sede_descripcion;
                    CargarMenu(UsuarioObj.Rol_id);
                    id_usuario = UsuarioObj.Pers_codigo;
                }
                else
                {
                    Session.Abandon();
                    FormsAuthentication.SignOut();
                    Response.Redirect("~/frmLogin.aspx");
                }

                string path = HttpContext.Current.Request.Url.AbsolutePath;
                EmpleadoBE objEmpleado = Session["LogUsuario"] as EmpleadoBE;
                Int32 cont = 0;
                foreach (OpcionBE menu in (new OpcionBL().ObtenerxRol(objEmpleado.Rol_id, 0)))
                {
                    if (SERVIDOR + menu.Opci_ruta == path || path == SERVIDOR + "/Principal/frmPrincipal.aspx")
                    {
                        cont++;
                    }
                    foreach (OpcionBE submenu in (new OpcionBL().ObtenerxRol(objEmpleado.Rol_id, menu.Opci_id)))
                    {
                        {
                            if (SERVIDOR + submenu.Opci_ruta == path)
                            {
                                cont++;
                            }
                        }
                    }
                }
                if (cont == 0)
                {
                    Response.Redirect("~/frmLogin.aspx");
                }
            }

        }

        public void CargarMenu(Int32 rol)
        {
            System.Text.StringBuilder sbOpcion = new System.Text.StringBuilder();
            sbOpcion.Append("<li class=\"active  treeview menu-open\">");
            sbOpcion.Append("<a href=\"" + Page.ResolveUrl("~" + "/Principal/frmPrincipal.aspx") + "\"><i class=\"fa fa-th-large\"></i><span>INICIO</span><span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span></a>");
            sbOpcion.Append("</li>");

            foreach (OpcionBE menu in (new OpcionBL().ObtenerxRol(rol, 0)))
            {
                sbOpcion.Append("<li class=\"treeview\">");
                if (menu.Opci_ruta == "#")
                {
                    sbOpcion.Append("<a href=\"" + menu.Opci_ruta + "\"><i class=\"fa "+ menu.Opci_Icono +" \"></i><span>" + menu.Opci_nombre + "</span><span class=\"pull-right-container\"></span></a>");
                }
                else
                {
                    sbOpcion.Append("<li><a href=\"" + Page.ResolveUrl("~" + menu.Opci_ruta) + "\">" + menu.Opci_nombre + "</a></li>");
                }

                sbOpcion.Append("<ul class=\"treeview-menu\">");
                foreach (OpcionBE submenu in (new OpcionBL().ObtenerxRol(rol, menu.Opci_id)))
                {
                    sbOpcion.Append("<li><a href=\"" + Page.ResolveUrl("~" + submenu.Opci_ruta) + "\">" + submenu.Opci_nombre + "</a></li>");
                }
                sbOpcion.Append("</ul>");
                sbOpcion.Append("</li>");
            }

            ltrMenu.Text = sbOpcion.ToString();
        }

        protected void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
    public class LoginInfo : frmMasterPage
    {
        object UsuarioObj = HttpContext.Current.Session["LogUsuario"];
        EmpleadoBE UsuarioBEObj;

        public LoginInfo()
        {
            if (HttpContext.Current.Session["LogUsuario"] != null)
            {
                UsuarioBEObj = HttpContext.Current.Session["LogUsuario"] as EmpleadoBE;
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
        }
    }
}